'use strict';
(function($){

    /**
     * Useful variables
     */
    var body = $('body'),
        html = $('html'),
        $window = $( window );


    /**
     * Debounce function to prevent jumping
     */
    var debounce=function(fn, delay) {
        var timer = null;
        return function () {
            var context = this, args = arguments;
            clearTimeout(timer);
            timer = setTimeout(function () {
                fn.apply(context, args);
            }, delay);
        };
    };

    $('.book-slider.book-slider0').slick({
        dots: false,
        infinite: true,
        autoplaySpeed: 3000,
        slidesToShow: 5,
        slidesToScroll: 1,
        autoplay: true,
        arrows: true,
        adaptiveHeight: false,
        appendArrows: $('.slickArrows0'),
        responsive: [
            {
                breakpoint: 768,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                }
            }
        ]
    });
    $('.book-slider.book-slider1').slick({
        dots: false,
        infinite: true,
        autoplaySpeed: 3000,
        slidesToShow: 5,
        slidesToScroll: 1,
        autoplay: true,
        arrows: true,
        adaptiveHeight: false,
        appendArrows: $('.slickArrows1'),
        responsive: [
            {
                breakpoint: 768,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                }
            }
        ]
    });
    $('.book-slider.book-slider2').slick({
        dots: false,
        infinite: true,
        autoplaySpeed: 3000,
        slidesToShow: 5,
        slidesToScroll: 1,
        autoplay: true,
        arrows: true,
        adaptiveHeight: false,
        appendArrows: $('.slickArrows2'),
        responsive: [
            {
                breakpoint: 768,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                }
            }
        ]
    });

    /**
     * slider hero
     * @type {boolean}
     */
    if ($('.home-page-slider .slide').length > 1) {
        $('.home-page-slider').slick({
            dots: false,
            infinite: false,
            slidesToShow: 1,
            slidesToScroll: 1,
            autoplay: true,
            asNavFor: '.slider-nav',
            responsive: [
                {
                    breakpoint: 768,
                    settings: {
                        slidesToShow:2
                    }
                },
                {
                    breakpoint: 568,
                    settings: {
                        slidesToShow:1
                    }
                }
            ]
        });
        $('.slider-nav').slick({
            slidesToShow: 4,
            slidesToScroll: 1,
            asNavFor: '.home-page-slider',
            dots: false,
            arrows: false,
            focusOnSelect: true,
            infinite: false,
            draggable:false,
            variableWidth: true,
        });
    }
    $('.home-page-slider-small').slick({
        dots: false,
        infinite: false,
        slidesToShow: 1,
        slidesToScroll: 1,
        autoplay: false,
        responsive: [
            {
                breakpoint: 768,
                settings: {
                    slidesToShow:2
                }
            },
            {
                breakpoint: 568,
                settings: {
                    slidesToShow:1
                }
            }
        ]
    });

    $('.commentsSlider').slick({
        dots: false,
        infinite: false,
        slidesToShow: 3,
        slidesToScroll: 1,
        autoplay: false,
        appendArrows: $('.slickArrowsComments'),
        responsive: [
            {
                breakpoint: 768,
                settings: {
                    slidesToShow: 2
                }
            },
            {
                breakpoint: 568,
                settings: {
                    slidesToShow:1
                }
            }

        ]
    });


    /**
     * Accordion data collapse
     */
    $('.collapse').on('show.bs.collapse', function (e) {
        $(e.target).parent().toggleClass('open');
    });
    $('.collapse').on('hide.bs.collapse', function (e) {
        $(e.target).parent().toggleClass('open');
    });

    /**
     * Change input type:file text
     */
    // $(":file").filestyle({
    //     input: false,
    //     text: 'Dodaj Załącznik',
    //     htmlIcon: '<i class="fa fa-plus"></i>',
    //     btnClass: "fileButton"
    // });
    $('input[type="checkbox"]').click(function() {
        if ($(this).is(':checked')) {
            $(this).find('label.check-box').addClass('checked');
        } else {
            $(this).find('label.check-box').removeClass('checked');
        }
    });



})(jQuery);