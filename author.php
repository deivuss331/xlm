<?php
$img="assets/img/";
$css="assets/css/";
$js="assets/js/";
?>

<!DOCTYPE html>
<html lang="pl">

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <title>Jacek Bartosiak</title>
    <meta name="description" content="Jacek Bartosiak">
    <meta name="keywords" content="jacek, bartosiak, ksiązka, blog">
    <meta name="author" content="AlbertK.">
    <meta http-equiv="X-Ua-Compatible" content="IE=edge">

    <link rel="stylesheet" href="<?php echo $css ?>bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo  $css ?>author.css">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,700&amp;subset=latin-ext" rel="stylesheet">

    <!--[if lt IE 9]>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.min.js"></script>
	<![endif]-->

</head>

<body>

    <main>

        <section class="author-header">
            <div class="container">
                <header>
                    <nav class="navbar navbar-dark bg-navbar navbar-expand-sm">
                        <a class="navbar-brand d-inline-block ml-2" style="color:black;" href="#"><i>JacekBartosiak.pl</i></a>
                        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#mainmenu" aria-controls="mainmenu" aria-expanded="false" aria-label="Przełącznik nawigacji">
                            <span class="navbar-toggler-icon navbar-dark"></span>
                        </button>
                        <div class="collapse navbar-collapse" id="mainmenu">
                            <button class="btn text-uppercase button d-inline-block ml-auto" type="button">ZAMÓW KSIĄŻKI</button>
                            </form>
                        </div>
                    </nav>
                </header>
            </div>
        </section>

        <section class="mod_1 author-bg-image">
            <div class="container">
                <div class="row">
                    <div class="col-md-6 bg-image-img">
                        <img class="img-fluid mt-5" src="<?php echo  $img ?>grupa.png" alt="audiobook">
                        <button class="btn button float-right mr-5 "><img src="assets/img/audiobook-icon.png" alt="" >ZAMÓW AUDIOBOOK</button>
                    </div>
                    <div class="col-md-6 ">
                        <div class="col-8 offset-2">
                            <img class="img-fluid mt-5 pb-md-2 pb-lg-0" src="<?php echo  $img ?>ksiazka.png" alt="ksiazka">
                        </div>
                        <button class="btn button float-left ml-5 mt-3 button-translate"><p>ZAMÓW KSIĄŻKI</p></button>
                    </div>

                </div>
                <div class="w-100 "></div>
                <div class="col-sm-12 text-center py-3 ">
                    <a href="#2"><img id="dosekcji2" width="31" height="86" src="<?php echo  $img ?>arrow_bottom.png " alt="strzałka w dół "></a>
                </div>
            </div>
        </section>

        <section class="mod_2" id="2">
            <div class="container ">
                <div class="row ">
                    <div class="col-md-6 ">
                        <img class="img-fluid rounded mt-4 mx-auto d-block" src="<?php echo  $img ?>profe.png" alt="jacek">
                    </div>
                    <div class="col-md-6 text-justify p-3 ">
                        <h1 class="h3 mt-5 font-weight-bold">dr Jacek Bartosiak</h1>
                        <p class="mt-5"> Senior Fellow w The Potomac Foundation w Waszyngtonie oraz Dyrektor Programu Gier Wojennych i Symulacji w Fundacji Pułaskiego. Adwokat i Partner Zarządzający w kancelarii adwokackiej zajmującej się obsługą biznesu. Współpracuje
                            na stałe z miesięcznikiem „Nowa Konfederacja”.</p>

                        <p>W The Potomac Foundation oraz w Fundacji Pułaskiego zajmuje się geostrategią, bezpieczeństwem oraz konfliktami zbrojnymi, w tym planowaniem przyszłego pola walki. Doktor nauk społecznych. Autor bestsellera roku 2016 - „Pacyfik
                            i Eurazja. O wojnie”.</p>
                    </div>
                </div>
            </div>
        </section>

        <section class="mod_2 ">
            <div class="container ">
                <div class="row ">
                    <div class="col-md-6">
                        <h3 class="text-center h3 font-weight-bold">Przeszłość jest prologiem</h3>
                        <ul class="list-group">
                            <li class="text-justify mt-5 list-group-item">
                                Czy w razie konfliktu Polska będzie celem dla Rosji?
                            </li>
                            <li class="text-justify list-group-item">
                                Czy możliwe jest odwrócenie sojuszy między USA, Polską i Rosją?
                            </li>
                            <li class="text-justify list-group-item">
                                Czy Chińczycy zakwestionują ład światowy?
                            </li>
                            <li class="text-justify list-group-item">
                                Na czym polega chińska strategia wobec USA?
                            </li>
                            <li class="text-justify list-group-item">
                                Dlaczego na naszych oczach rozpada się stary ład światowy i jak wykuwa się nowy porządek świata?
                            </li>
                            <li class="text-justify list-group-item">
                                Czy Amerykanie mają problem z Niemcami?
                            </li>
                            <li class="text-justify list-group-item">
                                Czy napięcie na Półwyspie Koreańskim rzutuje na sytuację w Europie Środkowej?
                            </li>
                            <li class="text-justify list-group-item">
                                Jaka była idea Piłsudskiego?
                            </li>
                            <li class="text-justify list-group-item">
                                Na co choruje Rosja?
                            </li>
                        </ul>
                        <button class="btn button mt-3 ml-3">ZAMÓW KSIĄŻKĘ</button>
                    </div>
                    <div class="col-md-6">
                        <img class="img-fluid rounded mt-md-5 pt-md-5 pt-lg-0  mt-lg-3" src="<?php echo  $img ?>double.png" alt="ksiazki">
                    </div>
                </div>
                <div class="row ">
                    <div class="col-12 text-center mt-5 pb-2 ">
                        <a href="#4"><img width="31" height="86" src="<?php echo  $img ?>arrow_bottom.png " alt="strzałka w dół "></a>
                    </div>
                </div>
            </div>
        </section>

        <section class="mod_2" id="4">
            <div class="container">
                <div class="row ">
                    <div class="col-md-6 ">
                        <img class="img-fluid rounded mt-md-5 pt-md-5 pt-lg-0  mt-lg-3" src="<?php echo  $img ?>ksiazki_2.png" alt="ksiazki">
                    </div>
                    <div class="col-md-6 ">
                        <h3 class="text-center h3 font-weight-bold">Rzeczpospolita<br> między lądem a morzem. <br>O wojnie i pokoju</h3>
                        <p class="text-justify mt-5"> Świat znalazł się na krawędzi zmian, które ukształtują nowy porządek XXI wieku. Zawirowania światowego systemu dotkną nas nieuchronnie.</p>

                        <p class="text-justify">Możliwe jest powstanie próżni bezpieczeństwa na wschód od naszych granic - wynikającej z rewizji ładu światowego. Coraz bardziej prawdopodobne staje się powstanie nowego systemu handlowo-komunikacyjnego zwanego Nowym Jedwabnym
                            Szlakiem. Realizacja tego projektu może powoli a skutecznie rozmontować hierarchiczną konstrukcję Zachodu, dyktującego warunki reszcie świata niezmiennie od kilkuset lat.</p>

                        <p class="text-justify">Dynamika i głębokość zmian globalnych wymusi na nas konieczność zrozumienia na nowo znaczenia przestrzeni w której żyjemy – obszarów położonych w sąsiedztwie, jak i tych pozornie dalekich, ale które wpływają na sukces lub porażkę
                            Rzeczpospolitej.
                        </p>
                        <button class="btn button mt-3">ZAMÓW KSIĄŻKĘ</button>
                    </div>
                </div>
            </div>
        </section>

        <section class="mod_2">
            <div class="container">
                <div class="row">
                    <div class="mt-5 col-10 offset-1 text-center embed-responsive embed-responsive-16by9 ">
                        <iframe class="embed-responsive-item mt-2 " src="https://www.youtube.com/embed/Xy4Bv51F4jM " allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture " allowfullscreen></iframe>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12 text-center mt-5 pb-2">
                        <a href="#"><img width="31" height="86" src="<?php echo  $img ?>arrow_bottom.png " alt="strzałka w dół "></a>
                    </div>
                </div>
            </div>
        </section>

        <section class="mod_2">
            <div class="container">
                <div class="row author-audiobook">
                    <div class="col-md-4 author-audiobook-order">
                        <h1>Rzeczpospolita między lądem a morzem. O wojnie i pokoju</h1>
                        <p>Audiobook książki już w sprzedaży!<br/>&nbsp;</p>
                        <button class="btn button mt-3"><img src="assets/img/audiobook-icon.png" alt="" >ZAMÓW AUDIOBOOK</button>
                    </div>
                    <div class="col-md-4 author-audiobook-img">
                        <img class="img-fluid mt-5" src="assets/img/author-tel-icon.png" alt="telefon">
                    </div>
                    <div class="col-md-4 author-audiobook-sample">
                        <img class="img-fluid" src="assets/img/author-audio-icon.png" alt="audiobook">
                        <h4>Odsłuchaj fragment audiobook-a</h4>
                        <button>ODSŁUCHAJ</button>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12 text-center mt-5 pb-2">
                        <a href="#"><img width="31" height="86" src="<?php echo  $img ?>arrow_bottom.png " alt="strzałka w dół "></a>
                    </div>
                </div>
            </div>
        </section>

        <section class="mod_2">
            <div class="container">
                <div class="column review">
                    <div class="d-flex review-box">
                        <img src="assets/img/review-1.png" alt='review' >
                        <p>"Ta ekscytująca nowa książka Jacka Bartosiaka jest zdecydowanie warta przeczytania. Jest wnikliwa i bardzo aktualna. Geostrategiczna sytuacja Polski w obliczu rewizjonistycznych tendencji Rosji sprawia, że zrozumienie geografii, sieci transportowych, trendów historycznych oraz znaczenia sojuszników jest ważniejsze niż kiedykolwiek."<br/><br/><span>General Frederick Benjamin „Ben” Hodges - były dowódca US Army (2014-2017) w Europie</span></p>
                    </div>
                    <div class="d-flex review-box">
                        <img src="assets/img/review-2.png" alt='review' >
                        <p>"Geopolityka, przestrzeń, interesy, wojny o wpływy i dominację, no i miejsce Polski w tym teatrze. To są kwestie rozważane w tej wielce osobistej, nie pozbawionej temperamentu książce. Sięgając do ojców geostrategii Autor - po raz kolejny - udziela nam obszernej lekcji. Można się z nim z godzić lub nie, ale jego z rozmachem prowadzone wywody przeczytać należy. Szczególnie, że to dzieło wychodzi w czasach burzliwych, z konsekwencjami trudnymi do przewidzenia. A dotyczy spraw najistotniejszych: naszej strategii i miejsca na Ziemi, o których tak mało w polskim publicznym i politycznym dyskursie. To wielkie wyzwanie dla królującej u nas doraźności."<br/><br/><span>Prof. Bogdan Góralczyk - autor tomu "Wielki Renesans. Chińska transformacja i jej konsekwencje".</span></p>
                    </div>
                </div>
                <div class="d-flex">
                    <button class="review-order-book">
                        ZAMÓW KSIĄŻKĘ
                    </button>
                </div>
                <div class="row">
                    <div class="col-12 text-center mt-5 pb-2">
                        <a href="#"><img width="31" height="86" src="<?php echo  $img ?>arrow_bottom.png " alt="strzałka w dół "></a>
                    </div>
                </div>
            </div>
        </section>

        <section class="mod_2">
            <div class="container">
                <div class="row review more-reviews">
                    <div class="col-md-4 review-box more-reviews-box">
                        <img src="assets/img/review-3.png" alt='review' >
                        <p>"Autor wykorzystując narzędzia analizy strategicznej w sposób niezwykle obrazowy przedstawia w ujęciu historycznym geopolityczne implikacje dla Polski. Kto zechce zrozumieć przesłanie autora uzbroi się w wiedzę kluczową dla naszej i europejskiej przyszłości."<br/><br/></p>
                        <span>Gen. Waldemar Skrzypczak - były dowódca wojsk lądowych i były wiceszef MON By Chancellery of the President of the Republic of Poland [GFDL 1.2 (https://gnu.org/licenses/old-licenses/fdl-1.2.html) or GFDL 1.2 (http://www.gnu.org/licenses/old-licenses/fdl-1.2.html)], via Wikimedia Commons</span>
                    </div>
                    <div class="col-md-4 review-box more-reviews-box">
                        <img src="assets/img/review-4.png" alt='review' >
                        <p>"Mam głęboką nadzieję że amerykańscy żołnierze przybywający do Polski będą mieli szansę przeczytać tę książkę aby zrozumieć to miejsce, nie tylko po to aby wiedzieli o co być może przyjdzie im walczyć ale żeby zapobiec konfliktom."<br/><br/></p>
                        <span>George Friedman - światowej sławy politolog i analityk zajmujący się geopolityką oraz stosunkami międzynarodowymi. Autor bestsellera New York Timesa „Następne 100 lat. Prognoza na XXI wiek” (ang. „The Next 100 Years”). By SorenKierkegaard [CC BY-SA 4.0 (https://creativecommons.org/licenses/by-sa/4.0)], from Wikimedia Commons</span>
                    </div>
                    <div class="col-md-4 review-box more-reviews-box">
                        <img src="assets/img/review-5.png" alt='review' >
                        <p>"Jacek Bartosiak sprawia, że pojęcia takie jak geopolityka i geostrategia wracają pod strzechy a Polacy zaczęli spoglądać na swój kraj i jego położenie na mapie jako kapitał, który można wykorzystać w celu efektywniejszego pozycjonowania Polski i bardziej świadomego dbania o interesy narodowe."<br/><br/></p>
                        <span>Krystian Zięć - legenda nowoczesnych Sil Powietrznych RP i były dowódca bazy F16 w Łasku By Jakub Sagan</span>
                    </div>
                    <div class="col-md-4 review-box more-reviews-box">
                        <img src="assets/img/review-6.png" alt='review' >
                        <p>"Widzieć więcej o otaczającej nas, komplikującej się bezustannie rzeczywistości polityczno-militarnej - to móc podejmować w krytycznych chwilach trafne decyzje. Jako bierni targani wichrami statyści, czy też świadomi ograniczeń, ale i możliwości partnerzy. Czy tę wiedzę zechcemy pozyskać, czy też ją zignorowć – zależ̇y od nas samych..."<br/><br/></p>
                        <span>Wojciech Łuczak - Wiceprezes Agencji Lotniczej, Altair wydawca miesięcznika RAPORT-wto i Magazynu Lotniczego Skrzydlata Polska</span>
                    </div>
                    <div class="col-md-4 review-box more-reviews-box">
                        <img src="assets/img/review-7.png" alt='review' >
                        <p>"Jacek Bartosiak jest wybitnym specjalistą od geopolitycznej geopolityki. Jego książki i wykłady wciągają nas w fascynującą geopolityczną grę i pozwalają być rozumiejącymi ją pionkami. A to i tak bardzo dużo, bo w tej grze olbrzymów o wyniku często przesądzają pionki. Polecam Jacka Bartosiaka jako przewodnika w tej geopolitycznej podróży."<br/><br/></p>
                        <span>Krzysztof Skowroński - Prezes Stowarzyszenia Dziennikarzy Polskich, redaktor naczelny Radia WNET By Adrian Grycuk [CC BY-SA 3.0 pl (https://creativecommons.org/licenses/by-sa/3.0/pl/deed.en)], from Wikimedia Commons</span>
                    </div>
                    <div class="col-md-4 review-box more-reviews-box">
                        <img src="assets/img/review-8.png" alt='review' >
                        <p>"Świat zmienia się fundamentalnie, a politycy, naukowcy i media odmawiają przyjęcia tego do wiadomości, tkwiąc głęboko w czasach minionych. Jacek Bartosiak jest jednym z chwalebnych wyjątków - otwiera oczy i objaśnia zachodząca przemianę. Ta książka zmienia wszystko, co przyzwyczailiście się myśleć o naszym miejscu w świecie."<br/><br/></p>
                        <span>Rafał Ziemkiewicz - pisarz i publicysta, współtwórca tygodnika "DoRzeczy" By Adrian Grycuk [CC BY-SA 3.0 pl (https://creativecommons.org/licenses/by-sa/3.0/pl/deed.en)], from Wikimedia Commons</span>
                    </div>
                </div>
                <div class="d-flex">
                    <button class="review-order-book">
                        ZAMÓW KSIĄŻKĘ
                    </button>
                </div>
                <div class="row">
                    <div class="col-12 text-center mt-5 pb-2">
                        <a href="#"><img width="31" height="86" src="<?php echo  $img ?>arrow_bottom.png " alt="strzałka w dół "></a>
                    </div>
                </div>
            </div>
        </section>

        <section class="mod_2">
            <div class="container">
                <div class="row short-desc">
                    <div class="col-lg-6 col-md-12">
                        <img src="assets/img/double-book-icon.png" alt='review' >
                    </div>
                    <div class="col-lg-6 col-md-12 d-flex short-desc-box flex-md-column">
                        <h1>Pacyfik i Eurazja. O Wojnie</h1>
                        <p>Niewykluczona jest niestety wojna dominacyjna, która z pewnością byłaby najpoważniejszym i największym konfliktem w świecie od czasu II wojny światowej. Jest to ważne dla Polski, członka NATO, bliskiego sojusznika USA, geograficznego sąsiada Rosji, położonego na planowanej trasie chińskiego Nowego Jedwabnego Szlaku, która - chcąc nie chcąc - będzie rachowana w kalkulacji układu/bilansu rywalizujących sił. W tym kontekście chciałem wyciągnąć lekcje z polskiej przeszłości i zrozumieć prawdziwe przyczyny rywalizacji dominacyjnych, by - jeśli to możliwe - uniknąć złych decyzji w obliczu ewentualnego konfliktu.</p>
                        <div class="d-flex">
                            <button class="review-order-book">
                                ZAMÓW KSIĄŻKĘ
                            </button>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12 text-center mt-5 pb-2">
                        <a href="#"><img width="31" height="86" src="<?php echo  $img ?>arrow_bottom.png " alt="strzałka w dół "></a>
                    </div>
                </div>
            </div>
        </section>

        <section class="mod_2">
            <div class="container order-books-container">
                <h1>Zamów książki już dziś</h1>
                <p>Wybierz klikając na okładkę</p>
                <div class="row order-books">
                    <div class="col-md-4 order-books-marg">
                        <div class="order-books-imgs">
                            <img src="assets/img/book-1.png" alt='book' >
                            <div class="order-books-form">
                                <img src="assets/img/book-icon.svg" alt='book-icon' >
                            </div>
                        </div>
                        <div class="d-flex order-books-title">
                            <span>Mayne Andrew</span>
                            <p>Naturalista</p>
                        </div>
                        <div class="d-flex justify-content-between order-books-price">
                            <div class="d-flex order-books-price-count">
                                <button>-</button>
                                <p>2</p>
                                <button>+</button>
                            </div>
                            <h5>29,99 zł</h5>
                            <div class="order-books-price-img">
                                <img src="assets/img/trolley-plus.svg" alt='cart' >
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 order-books-marg">
                        <div class="order-books-imgs">
                            <img src="assets/img/book-1.png" alt='book' >
                            <div class="order-books-form">
                                <img src="assets/img/ebook-icon.svg" alt='book-icon' >
                            </div>
                        </div>
                        <div class="d-flex order-books-title">
                            <span>Mayne Andrew</span>
                            <p>Naturalista</p>
                        </div>
                        <div class="d-flex justify-content-between order-books-price">
                            <div class="d-flex order-books-price-count">
                                <button>-</button>
                                <p>2</p>
                                <button>+</button>
                            </div>
                            <h5>29,99 zł</h5>
                            <div class="order-books-price-img">
                                <img src="assets/img/trolley-plus.svg" alt='cart' >
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 order-books-marg">
                        <div class="order-books-imgs">
                            <img src="assets/img/book-1.png" alt='book' >
                            <div class="order-books-form d-flex">
                                <img src="assets/img/book-icon.svg" alt='book-icon' >
                                <p>+</p>
                                <img src="assets/img/ebook-icon.svg" alt='book-icon' class="book-icon-last" >
                            </div>
                        </div>
                        <div class="d-flex order-books-title">
                            <span>Mayne Andrew</span>
                            <p>Naturalista</p>
                        </div>
                        <div class="d-flex justify-content-between order-books-price">
                            <div class="d-flex order-books-price-count">
                                <button>-</button>
                                <p>2</p>
                                <button>+</button>
                            </div>
                            <h5>29,99 zł</h5>
                            <div class="order-books-price-img">
                                <img src="assets/img/trolley-plus.svg" alt='cart' >
                            </div>
                        </div>
                    </div>

                    <div class="col-md-4 order-books-marg">
                        <div class="order-books-imgs">
                            <img src="assets/img/book-2.png" alt='book' >
                            <div class="order-books-form">
                                <img src="assets/img/book-icon.svg" alt='book-icon' >
                            </div>
                        </div>
                        <div class="d-flex order-books-title">
                            <span>Mayne Andrew</span>
                            <p>Naturalista</p>
                        </div>
                        <div class="d-flex justify-content-between order-books-price">
                            <div class="d-flex order-books-price-count">
                                <button>-</button>
                                <p>0</p>
                                <button>+</button>
                            </div>
                            <h5>29,99 zł</h5>
                            <div class="order-books-price-img">
                                <img src="assets/img/trolley-plus.svg" alt='cart' >
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 order-books-marg">
                        <div class="order-books-imgs">
                            <img src="assets/img/book-2.png" alt='book' >
                            <div class="order-books-form">
                                <img src="assets/img/ebook-icon.svg" alt='book-icon' >
                            </div>
                        </div>
                        <div class="d-flex order-books-title">
                            <span>Mayne Andrew</span>
                            <p>Naturalista</p>
                        </div>
                        <div class="d-flex justify-content-between order-books-price">
                            <div class="d-flex order-books-price-count">
                                <button>-</button>
                                <p>0</p>
                                <button>+</button>
                            </div>
                            <h5>29,99 zł</h5>
                            <div class="order-books-price-img">
                                <img src="assets/img/trolley-plus.svg" alt='cart' >
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 order-books-marg">
                        <div class="order-books-imgs">
                            <img src="assets/img/book-2.png" alt='book' >
                            <div class="order-books-form d-flex">
                                <img src="assets/img/book-icon.svg" alt='book-icon' >
                                <p>+</p>
                                <img src="assets/img/ebook-icon.svg" alt='book-icon' class="book-icon-last" >
                            </div>
                        </div>
                        <div class="d-flex order-books-title">
                            <span>Mayne Andrew</span>
                            <p>Naturalista</p>
                        </div>
                        <div class="d-flex justify-content-between order-books-price">
                            <div class="d-flex order-books-price-count">
                                <button>-</button>
                                <p>0</p>
                                <button>+</button>
                            </div>
                            <h5>29,99 zł</h5>
                            <div class="order-books-price-img">
                                <img src="assets/img/trolley-plus.svg" alt='cart' >
                            </div>
                        </div>
                    </div>

                    <div class="col-md-4 order-books-marg">
                        <div class="order-books-imgs">
                            <img src="assets/img/book-3.png" alt='book' >
                            <div class="order-books-form">
                                <img src="assets/img/book-icon.svg" alt='book-icon' >
                            </div>
                        </div>
                        <div class="d-flex order-books-title">
                            <span>Mayne Andrew</span>
                            <p>Naturalista</p>
                        </div>
                        <div class="d-flex justify-content-between order-books-price">
                            <div class="d-flex order-books-price-count">
                                <button>-</button>
                                <p>0</p>
                                <button>+</button>
                            </div>
                            <h5>29,99 zł</h5>
                            <div class="order-books-price-img">
                                <img src="assets/img/trolley-plus.svg" alt='cart' >
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 order-books-marg">
                        <div class="order-books-imgs">
                            <img src="assets/img/book-3.png" alt='book' >
                            <div class="order-books-form">
                                <img src="assets/img/ebook-icon.svg" alt='book-icon' >
                            </div>
                        </div>
                        <div class="d-flex order-books-title">
                            <span>Mayne Andrew</span>
                            <p>Naturalista</p>
                        </div>
                        <div class="d-flex justify-content-between order-books-price">
                            <div class="d-flex order-books-price-count">
                                <button>-</button>
                                <p>0</p>
                                <button>+</button>
                            </div>
                            <h5>29,99 zł</h5>
                            <div class="order-books-price-img">
                                <img src="assets/img/trolley-plus.svg" alt='cart' >
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 order-books-marg">
                        <div class="order-books-imgs">
                            <img src="assets/img/book-3.png" alt='book' >
                            <div class="order-books-form d-flex">
                                <img src="assets/img/book-icon.svg" alt='book-icon' >
                                <p>+</p>
                                <img src="assets/img/ebook-icon.svg" alt='book-icon' class="book-icon-last" >
                            </div>
                        </div>
                        <div class="d-flex order-books-title">
                            <span>Mayne Andrew</span>
                            <p>Naturalista</p>
                        </div>
                        <div class="d-flex justify-content-between order-books-price">
                            <div class="d-flex order-books-price-count">
                                <button>-</button>
                                <p>0</p>
                                <button>+</button>
                            </div>
                            <h5>29,99 zł</h5>
                            <div class="order-books-price-img">
                                <img src="assets/img/trolley-plus.svg" alt='cart' >
                            </div>
                        </div>
                    </div>

                    <div class="col-md-4 order-books-marg">
                        <div class="order-books-imgs">
                            <img src="assets/img/book-4.png" alt='book' >
                            <div class="order-books-form">
                                <img src="assets/img/book-icon.svg" alt='book-icon' >
                            </div>
                        </div>
                        <div class="d-flex order-books-title">
                            <span>Mayne Andrew</span>
                            <p>Naturalista</p>
                        </div>
                        <div class="d-flex justify-content-between order-books-price">
                            <div class="d-flex order-books-price-count">
                                <button>-</button>
                                <p>0</p>
                                <button>+</button>
                            </div>
                            <h5>29,99 zł</h5>
                            <div class="order-books-price-img">
                                <img src="assets/img/trolley-plus.svg" alt='cart' >
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 order-books-marg">
                        <div class="order-books-imgs">
                            <img src="assets/img/book-4.png" alt='book' >
                            <div class="order-books-form">
                                <img src="assets/img/ebook-icon.svg" alt='book-icon' >
                            </div>
                        </div>
                        <div class="d-flex order-books-title">
                            <span>Mayne Andrew</span>
                            <p>Naturalista</p>
                        </div>
                        <div class="d-flex justify-content-between order-books-price">
                            <div class="d-flex order-books-price-count">
                                <button>-</button>
                                <p>0</p>
                                <button>+</button>
                            </div>
                            <h5>29,99 zł</h5>
                            <div class="order-books-price-img">
                                <img src="assets/img/trolley-plus.svg" alt='cart' >
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 order-books-marg">
                        <div class="order-books-imgs">
                            <img src="assets/img/book-4.png" alt='book' >
                            <div class="order-books-form d-flex">
                                <img src="assets/img/book-icon.svg" alt='book-icon' >
                                <p>+</p>
                                <img src="assets/img/ebook-icon.svg" alt='book-icon' class="book-icon-last" >
                            </div>
                        </div>
                        <div class="d-flex order-books-title">
                            <span>Mayne Andrew</span>
                            <p>Naturalista</p>
                        </div>
                        <div class="d-flex justify-content-between order-books-price">
                            <div class="d-flex order-books-price-count">
                                <button>-</button>
                                <p>0</p>
                                <button>+</button>
                            </div>
                            <h5>29,99 zł</h5>
                            <div class="order-books-price-img">
                                <img src="assets/img/trolley-plus.svg" alt='cart' >
                            </div>
                        </div>
                    </div>

                    <div class="col-md-4 order-books-marg">
                        <div class="order-books-imgs">
                            <img src="assets/img/book-5.png" alt='book' >
                            <div class="order-books-form">
                                <img src="assets/img/book-icon.svg" alt='book-icon' >
                            </div>
                        </div>
                        <div class="d-flex order-books-title">
                            <span>Mayne Andrew</span>
                            <p>Naturalista</p>
                        </div>
                        <div class="d-flex justify-content-between order-books-price">
                            <div class="d-flex order-books-price-count">
                                <button>-</button>
                                <p>0</p>
                                <button>+</button>
                            </div>
                            <h5>29,99 zł</h5>
                            <div class="order-books-price-img">
                                <img src="assets/img/trolley-plus.svg" alt='cart' >
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 order-books-marg">
                        <div class="order-books-imgs">
                            <img src="assets/img/book-5.png" alt='book' >
                            <div class="order-books-form">
                                <img src="assets/img/ebook-icon.svg" alt='book-icon' >
                            </div>
                        </div>
                        <div class="d-flex order-books-title">
                            <span>Mayne Andrew</span>
                            <p>Naturalista</p>
                        </div>
                        <div class="d-flex justify-content-between order-books-price">
                            <div class="d-flex order-books-price-count">
                                <button>-</button>
                                <p>0</p>
                                <button>+</button>
                            </div>
                            <h5>29,99 zł</h5>
                            <div class="order-books-price-img">
                                <img src="assets/img/trolley-plus.svg" alt='cart' >
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="mod_2">
            <div class="container">
                <div class="column sub-cart">
                    <h2>Zestawienie wyboru</h2>
                    <div class="d-flex sub-cart-text">
                        <div class="sub-cart-text-first">
                            <span>Książka</span>
                            <p>Rzeczpospolita między lądem a morzem</p>
                        </div>
                        <div class="d-flex sub-cart-price-count">
                            <button>-</button>
                                <p>2</p>
                            <button>+</button>
                        </div>
                        <div class="sub-cart-text-second d-flex">
                            2 x 29,99 zł
                            <img src="assets/img/close-icon.svg" alt='close' class="sub-cart-text-icon" >
                        </div>
                    </div>
                    <div class="d-flex">
                        <button class="review-order-book">
                            ZAMAWIAM I PRZECHODZĘ DO KOSZYKA
                        </button>
                    </div>
                </div>
            </div>
        </section>

        <section class="mod-2 author-footer-container">
            <div class="container">
                <div class="author-footer">
                    <div class="d-flex social-icons">
                        <a class="text-center d-xl-flex justify-content-xl-center align-items-xl-center" href="#">
                            <img src="./assets/img/facebook.svg">
                        </a>
                        <a class="text-center d-xl-flex justify-content-xl-center align-items-xl-center" href="#">
                            <img src="./assets/img/instagram.svg">
                        </a>
                        <a class="d-xl-flex justify-content-xl-center align-items-xl-center" href="#">
                            <img src="./assets/img/youtube%20(1).svg">
                        </a>
                        <a class="d-xl-flex justify-content-xl-center align-items-xl-center" href="#">
                            <img src="./assets/img/twitter%20(2).svg">
                        </a>
                    </div>
                    <p>MIGOMEDIA</p>
                </div>
            </div>
        </section>
    </main>

    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js " integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo " crossorigin="anonymous "></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js " integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49 " crossorigin="anonymous "></script>

    <script src="<?php echo  $js ?>bootstrap.min.js "></script>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.js"></script>

    <script>
        $('a[href*="#"]').on('click', function(e) {
            e.preventDefault()

            $('html, body').animate({
                    scrollTop: $($(this).attr('href')).offset().top,
                },
                500,
                'linear'
            )
        });
    </script>

</body>

</html>