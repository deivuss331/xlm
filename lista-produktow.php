<?php


include 'modules/header.php';?>
<div class="products-list">
    <?
    include 'modules/breadcrumbs.php';
    include 'modules/header-section.php';
    ?>

    <div class="container">
        <div class="row">
            <div class="col-md-2dot4">
                <div class="filters">
                    <h4>Filtruj wyniki:</h4>
                    <form action="">
                        <h5>Wydawnictwo</h5>
                        <div class="form-check">
                            <input class="form-check-input" type="checkbox" value="" id="defaultCheck1">
                            <label class="check-box form-check-label" for="defaultCheck1">
                            </label>
                            <div class="label">Alternatywy</div>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" type="checkbox" value="" id="defaultCheck2">
                            <label class="check-box form-check-label" for="defaultCheck2">
                            </label>
                            <div class="label">Jedność</div>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" type="checkbox" value="" id="defaultCheck3">
                            <label class="check-box form-check-label" for="defaultCheck3">
                            </label>
                            <div class="label">Biały Kruk</div>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" type="checkbox" value="" id="defaultCheck4">
                            <label class="check-box form-check-label" for="defaultCheck4">
                            </label>
                            <div class="label">PWN</div>
                        </div>
                        <a href="#">wszystkie wydawnictwa</a>
                        <hr>
                        <h5>Cena</h5>
                        <div class="input-group">
                            <input type="text" class="form-control" placeholder="min">
                            <div class="p-2">-</div>
                            <input type="text" class="form-control" placeholder="max">
                        </div>
                        <hr>
                        <h5>Status</h5>
                        <div class="form-check">
                            <input class="form-check-input" type="checkbox" value="" id="defaultCheck1s">
                            <label class="check-box form-check-label" for="defaultCheck1s">
                            </label>
                            <div class="label"><div class="badge badge-success">Nowość</div></div>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" type="checkbox" value="" id="defaultCheck2s">
                            <label class="check-box form-check-label" for="defaultCheck2s">
                            </label>
                            <div class="label"><div class="badge badge-info">Bestseller</div></div>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" type="checkbox" value="" id="defaultCheck3s">
                            <label class="check-box form-check-label" for="defaultCheck3s">
                            </label>
                            <div class="label"><div class="badge badge-danger">Promocja</div></div>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" type="checkbox" value="" id="defaultCheck4s">
                            <label class="check-box form-check-label" for="defaultCheck4s">
                            </label>
                            <div class="label"><div class="badge badge-warning">Przedsprzedaż</div></div>
                        </div>
                        <hr>
                        <h5>Autorzy</h5>
                        <div class="form-check">
                            <input class="form-check-input" type="checkbox" value="" id="defaultCheck1a">
                            <label class="check-box form-check-label" for="defaultCheck1a">
                            </label>
                            <div class="label">Joanna Bator</div>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" type="checkbox" value="" id="defaultCheck2a">
                            <label class="check-box form-check-label" for="defaultCheck2a">
                            </label>
                            <div class="label">Jacek Dehnel</div>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" type="checkbox" value="" id="defaultCheck3a">
                            <label class="check-box form-check-label" for="defaultCheck3a">
                            </label>
                            <div class="label">Krzysztof Jedliński</div>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" type="checkbox" value="" id="defaultCheck4a">
                            <label class="check-box form-check-label" for="defaultCheck4a">
                            </label>
                            <div class="label">Barbara Kosmowska</div>
                        </div>
                        <a href="#">wszyscy autorzy</a>
                        <hr>
                        <button type="submit" class="button">FILTRUJ WYNIKI</button>
                    </form>
                    <div class="top10">
                        <h4>Top 10:</h4>
                        <div class="card">
                            <img src="assets/img/top10.png" class="card-img" alt="...">
                            <div class="card-img-overlay">
                                <div class="badge badge-success">N</div>
                            </div>
                            <div class="card-body">
                                <p class="card-text">Praca zbiorowa</p>
                                <h5 class="card-title">Atlas patologii
                                    złośliwych nowo...</h5>
                                <div class="card-footer">
                                    <div class="price">29,99 zł <small>32,00 zł</small></div>
                                    <a href="#" class="position-relative">
                                        <img src="assets/img/trolley.svg" alt="cart icon">
                                        <span class="badge badge-custom small">+</span>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <img src="assets/img/top10.png" class="card-img" alt="...">
                            <div class="card-img-overlay">
                                <div class="badge badge-warning">P</div>
                            </div>
                            <div class="card-body">
                                <p class="card-text">Praca zbiorowa</p>
                                <h5 class="card-title">Atlas patologii
                                    złośliwych nowo...</h5>
                                <div class="card-footer">
                                    <div class="price">29,99 zł</div>
                                    <a href="#" class="position-relative">
                                        <img src="assets/img/trolley.svg" alt="cart icon">
                                        <span class="badge badge-custom small">+</span>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <img src="assets/img/top10.png" class="card-img" alt="...">
                            <div class="card-img-overlay">
                                <div class="badge badge-info">B</div>
                            </div>
                            <div class="card-body">
                                <p class="card-text">Praca zbiorowa</p>
                                <h5 class="card-title">Atlas patologii
                                    złośliwych nowo...</h5>
                                <div class="card-footer">
                                    <div class="price">29,99 zł</div>
                                    <a href="#" class="position-relative">
                                        <img src="assets/img/trolley.svg" alt="cart icon">
                                        <span class="badge badge-custom small">+</span>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <img src="assets/img/top10.png" class="card-img" alt="...">
                            <div class="card-img-overlay">
                                <div class="badge badge-success">N</div>
                            </div>
                            <div class="card-body">
                                <p class="card-text">Praca zbiorowa</p>
                                <h5 class="card-title">Atlas patologii
                                    złośliwych nowo...</h5>
                                <div class="card-footer">
                                    <div class="price">29,99 zł <small>32,00 zł</small></div>
                                    <a href="#" class="position-relative">
                                        <img src="assets/img/trolley.svg" alt="cart icon">
                                        <span class="badge badge-custom small">+</span>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <img src="assets/img/top10.png" class="card-img" alt="...">
                            <div class="card-img-overlay">
                                <div class="badge badge-warning">P</div>
                            </div>
                            <div class="card-body">
                                <p class="card-text">Praca zbiorowa</p>
                                <h5 class="card-title">Atlas patologii
                                    złośliwych nowo...</h5>
                                <div class="card-footer">
                                    <div class="price">29,99 zł</div>
                                    <a href="#" class="position-relative">
                                        <img src="assets/img/trolley.svg" alt="cart icon">
                                        <span class="badge badge-custom small">+</span>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <img src="assets/img/top10.png" class="card-img" alt="...">
                            <div class="card-img-overlay">
                                <div class="badge badge-info">B</div>
                            </div>
                            <div class="card-body">
                                <p class="card-text">Praca zbiorowa</p>
                                <h5 class="card-title">Atlas patologii
                                    złośliwych nowo...</h5>
                                <div class="card-footer">
                                    <div class="price">29,99 zł</div>
                                    <a href="#" class="position-relative">
                                        <img src="assets/img/trolley.svg" alt="cart icon">
                                        <span class="badge badge-custom small">+</span>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <img src="assets/img/top10.png" class="card-img" alt="...">
                            <div class="card-img-overlay">
                                <div class="badge badge-success">N</div>
                            </div>
                            <div class="card-body">
                                <p class="card-text">Praca zbiorowa</p>
                                <h5 class="card-title">Atlas patologii
                                    złośliwych nowo...</h5>
                                <div class="card-footer">
                                    <div class="price">29,99 zł <small>32,00 zł</small></div>
                                    <a href="#" class="position-relative">
                                        <img src="assets/img/trolley.svg" alt="cart icon">
                                        <span class="badge badge-custom small">+</span>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-10dot4">
                <div class="desc">
                    <p>Solidna dawka specjalistycznej wiedzy z dziedziny medycyny. Anatomia, anestezjologia, chirurgia, psychologia, dermatologia, seksuologia. Radiologia, kardiologia, diabetologia, fizjoterapia, pediatria. Mukowiscydoza, refluks, choroby wewnętrzne, alergia, migrena i wiele innych. Zdrowe żywienie i zdrowy tryb życia. Poznaj swój organizm i zwiększ świadomość na temat współczesnych metod leczenia.</p>
                </div>

                <div class="row book-cards">
                    <div class="col-md-3">
                        <div class="card">
                            <img src="assets/img/book3.png" class="card-img" alt="...">
                            <div class="card-img-overlay">
                                <div class="row">
                                    <div class="col-4">
                                        <a href="#" class="hearth">
                                            <i class="fa fa-heart-o" aria-hidden="true"></i>
                                            <span>
                                Do schowka
                            </span>
                                        </a>

                                    </div>
                                    <div class="col-8 text-right">
                                        <div class="badge badge-success">Nowość</div>
                                        <div class="badge badge-danger">Promocja</div>
                                        <div class="badge badge-warning">Przedsprzedaż</div>
                                        <div class="badge badge-info">Bestseller</div>
                                    </div>
                                </div>
                            </div>
                            <div class="card-body">
                                <p class="card-text">Mayne Andrew</p>
                                <h5 class="card-title">Naturalista</h5>
                                <div class="card-footer">
                                    <div class="price">29,99 zł</div>
                                    <a href="#" class="position-relative">
                                        <img src="assets/img/trolley.svg" alt="cart icon">
                                        <span class="badge badge-custom small">+</span>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="card">
                            <img src="assets/img/book3.png" class="card-img" alt="...">
                            <div class="card-img-overlay">
                                <div class="row">
                                    <div class="col-4">
                                        <a href="#" class="hearth">
                                            <i class="fa fa-heart-o" aria-hidden="true"></i>
                                            <span>
                                Do schowka
                            </span>
                                        </a>

                                    </div>
                                    <div class="col-8 text-right">
                                        <div class="badge badge-success">Nowość</div>
                                        <div class="badge badge-danger">Promocja</div>
                                        <div class="badge badge-warning">Przedsprzedaż</div>
                                        <div class="badge badge-info">Bestseller</div>
                                    </div>
                                </div>
                            </div>
                            <div class="card-body">
                                <p class="card-text">Mayne Andrew</p>
                                <h5 class="card-title">Naturalista</h5>
                                <div class="card-footer">
                                    <div class="price">29,99 zł</div>
                                    <a href="#" class="position-relative">
                                        <img src="assets/img/trolley.svg" alt="cart icon">
                                        <span class="badge badge-custom small">+</span>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="card">
                            <img src="assets/img/book3.png" class="card-img" alt="...">
                            <div class="card-img-overlay">
                                <div class="row">
                                    <div class="col-4">
                                        <a href="#" class="hearth">
                                            <i class="fa fa-heart-o" aria-hidden="true"></i>
                                            <span>
                                Do schowka
                            </span>
                                        </a>

                                    </div>
                                    <div class="col-8 text-right">
                                        <div class="badge badge-success">Nowość</div>
                                        <div class="badge badge-danger">Promocja</div>
                                        <div class="badge badge-warning">Przedsprzedaż</div>
                                        <div class="badge badge-info">Bestseller</div>
                                    </div>
                                </div>
                            </div>
                            <div class="card-body">
                                <p class="card-text">Mayne Andrew</p>
                                <h5 class="card-title">Naturalista</h5>
                                <div class="card-footer">
                                    <div class="price">29,99 zł</div>
                                    <a href="#" class="position-relative">
                                        <img src="assets/img/trolley.svg" alt="cart icon">
                                        <span class="badge badge-custom small">+</span>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="card">
                            <img src="assets/img/book3.png" class="card-img" alt="...">
                            <div class="card-img-overlay">
                                <div class="row">
                                    <div class="col-4">
                                        <a href="#" class="hearth">
                                            <i class="fa fa-heart-o" aria-hidden="true"></i>
                                            <span>
                                Do schowka
                            </span>
                                        </a>

                                    </div>
                                    <div class="col-8 text-right">
                                        <div class="badge badge-success">Nowość</div>
                                        <div class="badge badge-danger">Promocja</div>
                                        <div class="badge badge-warning">Przedsprzedaż</div>
                                        <div class="badge badge-info">Bestseller</div>
                                    </div>
                                </div>
                            </div>
                            <div class="card-body">
                                <p class="card-text">Mayne Andrew</p>
                                <h5 class="card-title">Naturalista</h5>
                                <div class="card-footer">
                                    <div class="price">29,99 zł</div>
                                    <a href="#" class="position-relative">
                                        <img src="assets/img/trolley.svg" alt="cart icon">
                                        <span class="badge badge-custom small">+</span>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="card">
                            <img src="assets/img/book3.png" class="card-img" alt="...">
                            <div class="card-img-overlay">
                                <div class="row">
                                    <div class="col-4">
                                        <a href="#" class="hearth">
                                            <i class="fa fa-heart-o" aria-hidden="true"></i>
                                            <span>
                                Do schowka
                            </span>
                                        </a>

                                    </div>
                                    <div class="col-8 text-right">
                                        <div class="badge badge-success">Nowość</div>
                                        <div class="badge badge-danger">Promocja</div>
                                        <div class="badge badge-warning">Przedsprzedaż</div>
                                        <div class="badge badge-info">Bestseller</div>
                                    </div>
                                </div>
                            </div>
                            <div class="card-body">
                                <p class="card-text">Mayne Andrew</p>
                                <h5 class="card-title">Naturalista</h5>
                                <div class="card-footer">
                                    <div class="price">29,99 zł</div>
                                    <a href="#" class="position-relative">
                                        <img src="assets/img/trolley.svg" alt="cart icon">
                                        <span class="badge badge-custom small">+</span>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="card">
                            <img src="assets/img/book3.png" class="card-img" alt="...">
                            <div class="card-img-overlay">
                                <div class="row">
                                    <div class="col-4">
                                        <a href="#" class="hearth">
                                            <i class="fa fa-heart-o" aria-hidden="true"></i>
                                            <span>
                                Do schowka
                            </span>
                                        </a>

                                    </div>
                                    <div class="col-8 text-right">
                                        <div class="badge badge-success">Nowość</div>
                                        <div class="badge badge-danger">Promocja</div>
                                        <div class="badge badge-warning">Przedsprzedaż</div>
                                        <div class="badge badge-info">Bestseller</div>
                                    </div>
                                </div>
                            </div>
                            <div class="card-body">
                                <p class="card-text">Mayne Andrew</p>
                                <h5 class="card-title">Naturalista</h5>
                                <div class="card-footer">
                                    <div class="price">29,99 zł</div>
                                    <a href="#" class="position-relative">
                                        <img src="assets/img/trolley.svg" alt="cart icon">
                                        <span class="badge badge-custom small">+</span>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="card">
                            <img src="assets/img/book3.png" class="card-img" alt="...">
                            <div class="card-img-overlay">
                                <div class="row">
                                    <div class="col-4">
                                        <a href="#" class="hearth">
                                            <i class="fa fa-heart-o" aria-hidden="true"></i>
                                            <span>
                                Do schowka
                            </span>
                                        </a>

                                    </div>
                                    <div class="col-8 text-right">
                                        <div class="badge badge-success">Nowość</div>
                                        <div class="badge badge-danger">Promocja</div>
                                        <div class="badge badge-warning">Przedsprzedaż</div>
                                        <div class="badge badge-info">Bestseller</div>
                                    </div>
                                </div>
                            </div>
                            <div class="card-body">
                                <p class="card-text">Mayne Andrew</p>
                                <h5 class="card-title">Naturalista</h5>
                                <div class="card-footer">
                                    <div class="price">29,99 zł</div>
                                    <a href="#" class="position-relative">
                                        <img src="assets/img/trolley.svg" alt="cart icon">
                                        <span class="badge badge-custom small">+</span>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="card">
                            <img src="assets/img/book3.png" class="card-img" alt="...">
                            <div class="card-img-overlay">
                                <div class="row">
                                    <div class="col-4">
                                        <a href="#" class="hearth">
                                            <i class="fa fa-heart-o" aria-hidden="true"></i>
                                            <span>
                                Do schowka
                            </span>
                                        </a>

                                    </div>
                                    <div class="col-8 text-right">
                                        <div class="badge badge-success">Nowość</div>
                                        <div class="badge badge-danger">Promocja</div>
                                        <div class="badge badge-warning">Przedsprzedaż</div>
                                        <div class="badge badge-info">Bestseller</div>
                                    </div>
                                </div>
                            </div>
                            <div class="card-body">
                                <p class="card-text">Mayne Andrew</p>
                                <h5 class="card-title">Naturalista</h5>
                                <div class="card-footer">
                                    <div class="price">29,99 zł</div>
                                    <a href="#" class="position-relative">
                                        <img src="assets/img/trolley.svg" alt="cart icon">
                                        <span class="badge badge-custom small">+</span>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="card">
                            <img src="assets/img/book3.png" class="card-img" alt="...">
                            <div class="card-img-overlay">
                                <div class="row">
                                    <div class="col-4">
                                        <a href="#" class="hearth">
                                            <i class="fa fa-heart-o" aria-hidden="true"></i>
                                            <span>
                                Do schowka
                            </span>
                                        </a>

                                    </div>
                                    <div class="col-8 text-right">
                                        <div class="badge badge-success">Nowość</div>
                                        <div class="badge badge-danger">Promocja</div>
                                        <div class="badge badge-warning">Przedsprzedaż</div>
                                        <div class="badge badge-info">Bestseller</div>
                                    </div>
                                </div>
                            </div>
                            <div class="card-body">
                                <p class="card-text">Mayne Andrew</p>
                                <h5 class="card-title">Naturalista</h5>
                                <div class="card-footer">
                                    <div class="price">29,99 zł</div>
                                    <a href="#" class="position-relative">
                                        <img src="assets/img/trolley.svg" alt="cart icon">
                                        <span class="badge badge-custom small">+</span>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="card">
                            <img src="assets/img/book3.png" class="card-img" alt="...">
                            <div class="card-img-overlay">
                                <div class="row">
                                    <div class="col-4">
                                        <a href="#" class="hearth">
                                            <i class="fa fa-heart-o" aria-hidden="true"></i>
                                            <span>
                                Do schowka
                            </span>
                                        </a>

                                    </div>
                                    <div class="col-8 text-right">
                                        <div class="badge badge-success">Nowość</div>
                                        <div class="badge badge-danger">Promocja</div>
                                        <div class="badge badge-warning">Przedsprzedaż</div>
                                        <div class="badge badge-info">Bestseller</div>
                                    </div>
                                </div>
                            </div>
                            <div class="card-body">
                                <p class="card-text">Mayne Andrew</p>
                                <h5 class="card-title">Naturalista</h5>
                                <div class="card-footer">
                                    <div class="price">29,99 zł</div>
                                    <a href="#" class="position-relative">
                                        <img src="assets/img/trolley.svg" alt="cart icon">
                                        <span class="badge badge-custom small">+</span>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="card">
                            <img src="assets/img/book3.png" class="card-img" alt="...">
                            <div class="card-img-overlay">
                                <div class="row">
                                    <div class="col-4">
                                        <a href="#" class="hearth">
                                            <i class="fa fa-heart-o" aria-hidden="true"></i>
                                            <span>
                                Do schowka
                            </span>
                                        </a>

                                    </div>
                                    <div class="col-8 text-right">
                                        <div class="badge badge-success">Nowość</div>
                                        <div class="badge badge-danger">Promocja</div>
                                        <div class="badge badge-warning">Przedsprzedaż</div>
                                        <div class="badge badge-info">Bestseller</div>
                                    </div>
                                </div>
                            </div>
                            <div class="card-body">
                                <p class="card-text">Mayne Andrew</p>
                                <h5 class="card-title">Naturalista</h5>
                                <div class="card-footer">
                                    <div class="price">29,99 zł</div>
                                    <a href="#" class="position-relative">
                                        <img src="assets/img/trolley.svg" alt="cart icon">
                                        <span class="badge badge-custom small">+</span>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="card">
                            <img src="assets/img/book3.png" class="card-img" alt="...">
                            <div class="card-img-overlay">
                                <div class="row">
                                    <div class="col-4">
                                        <a href="#" class="hearth">
                                            <i class="fa fa-heart-o" aria-hidden="true"></i>
                                            <span>
                                Do schowka
                            </span>
                                        </a>

                                    </div>
                                    <div class="col-8 text-right">
                                        <div class="badge badge-success">Nowość</div>
                                        <div class="badge badge-danger">Promocja</div>
                                        <div class="badge badge-warning">Przedsprzedaż</div>
                                        <div class="badge badge-info">Bestseller</div>
                                    </div>
                                </div>
                            </div>
                            <div class="card-body">
                                <p class="card-text">Mayne Andrew</p>
                                <h5 class="card-title">Naturalista</h5>
                                <div class="card-footer">
                                    <div class="price">29,99 zł</div>
                                    <a href="#" class="position-relative">
                                        <img src="assets/img/trolley.svg" alt="cart icon">
                                        <span class="badge badge-custom small">+</span>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="card">
                            <img src="assets/img/book3.png" class="card-img" alt="...">
                            <div class="card-img-overlay">
                                <div class="row">
                                    <div class="col-4">
                                        <a href="#" class="hearth">
                                            <i class="fa fa-heart-o" aria-hidden="true"></i>
                                            <span>
                                Do schowka
                            </span>
                                        </a>

                                    </div>
                                    <div class="col-8 text-right">
                                        <div class="badge badge-success">Nowość</div>
                                        <div class="badge badge-danger">Promocja</div>
                                        <div class="badge badge-warning">Przedsprzedaż</div>
                                        <div class="badge badge-info">Bestseller</div>
                                    </div>
                                </div>
                            </div>
                            <div class="card-body">
                                <p class="card-text">Mayne Andrew</p>
                                <h5 class="card-title">Naturalista</h5>
                                <div class="card-footer">
                                    <div class="projector_number" id="projector_number_cont">
                                        <button id="projector_number_down" class="projector_number_down" type="button">
                                            <i class="fa fa-minus" aria-hidden="true"></i>
                                        </button>
                                        <input class="projector_number" name="number" id="projector_number" value="1" readonly="readonly">
                                        <button id="projector_number_up" class="projector_number_up" type="button">
                                            <i class="fa fa-plus" aria-hidden="true"></i>
                                        </button>
                                    </div>
                                    <div class="price">29,99 zł</div>
                                    <a href="#" class="position-relative">
                                        <img src="assets/img/trolley.svg" alt="cart icon">
                                        <span class="badge badge-custom small">+</span>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="card">
                            <img src="assets/img/book3.png" class="card-img" alt="...">
                            <div class="card-img-overlay">
                                <div class="row">
                                    <div class="col-4">
                                        <a href="#" class="hearth">
                                            <i class="fa fa-heart-o" aria-hidden="true"></i>
                                            <span>
                                Do schowka
                            </span>
                                        </a>

                                    </div>
                                    <div class="col-8 text-right">
                                        <div class="badge badge-success">Nowość</div>
                                        <div class="badge badge-danger">Promocja</div>
                                        <div class="badge badge-warning">Przedsprzedaż</div>
                                        <div class="badge badge-info">Bestseller</div>
                                    </div>
                                </div>
                            </div>
                            <div class="card-body">
                                <p class="card-text">Mayne Andrew</p>
                                <h5 class="card-title">Naturalista</h5>
                                <div class="card-footer">
                                    <div class="projector_number" id="projector_number_cont">
                                        <button id="projector_number_down" class="projector_number_down" type="button">
                                            <i class="fa fa-minus" aria-hidden="true"></i>
                                        </button>
                                        <input class="projector_number" name="number" id="projector_number" value="1" readonly="readonly">
                                        <button id="projector_number_up" class="projector_number_up" type="button">
                                            <i class="fa fa-plus" aria-hidden="true"></i>
                                        </button>
                                    </div>
                                    <div class="price">29,99 zł</div>
                                    <a href="#" class="position-relative">
                                        <img src="assets/img/trolley.svg" alt="cart icon">
                                        <span class="badge badge-custom small">+</span>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="card">
                            <img src="assets/img/book3.png" class="card-img" alt="...">
                            <div class="card-img-overlay">
                                <div class="row">
                                    <div class="col-4">
                                        <a href="#" class="hearth">
                                            <i class="fa fa-heart-o" aria-hidden="true"></i>
                                            <span>
                                Do schowka
                            </span>
                                        </a>

                                    </div>
                                    <div class="col-8 text-right">
                                        <div class="badge badge-success">Nowość</div>
                                        <div class="badge badge-danger">Promocja</div>
                                        <div class="badge badge-warning">Przedsprzedaż</div>
                                        <div class="badge badge-info">Bestseller</div>
                                    </div>
                                </div>
                            </div>
                            <div class="card-body">
                                <p class="card-text">Mayne Andrew</p>
                                <h5 class="card-title">Naturalista</h5>
                                <div class="card-footer">
                                    <div class="projector_number" id="projector_number_cont">
                                        <button id="projector_number_down" class="projector_number_down" type="button">
                                            <i class="fa fa-minus" aria-hidden="true"></i>
                                        </button>
                                        <input class="projector_number" name="number" id="projector_number" value="1" readonly="readonly">
                                        <button id="projector_number_up" class="projector_number_up" type="button">
                                            <i class="fa fa-plus" aria-hidden="true"></i>
                                        </button>
                                    </div>
                                    <div class="price">29,99 zł</div>
                                    <a href="#" class="position-relative">
                                        <img src="assets/img/trolley.svg" alt="cart icon">
                                        <span class="badge badge-custom small">+</span>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="card">
                            <img src="assets/img/book3.png" class="card-img" alt="...">
                            <div class="card-img-overlay">
                                <div class="row">
                                    <div class="col-4">
                                        <a href="#" class="hearth">
                                            <i class="fa fa-heart-o" aria-hidden="true"></i>
                                            <span>
                                Do schowka
                            </span>
                                        </a>

                                    </div>
                                    <div class="col-8 text-right">
                                        <div class="badge badge-success">Nowość</div>
                                        <div class="badge badge-danger">Promocja</div>
                                        <div class="badge badge-warning">Przedsprzedaż</div>
                                        <div class="badge badge-info">Bestseller</div>
                                    </div>
                                </div>
                            </div>
                            <div class="card-body">
                                <p class="card-text">Mayne Andrew</p>
                                <h5 class="card-title">Naturalista</h5>
                                <div class="card-footer">
                                    <div class="projector_number" id="projector_number_cont">
                                        <button id="projector_number_down" class="projector_number_down" type="button">
                                            <i class="fa fa-minus" aria-hidden="true"></i>
                                        </button>
                                        <input class="projector_number" name="number" id="projector_number" value="1" readonly="readonly">
                                        <button id="projector_number_up" class="projector_number_up" type="button">
                                            <i class="fa fa-plus" aria-hidden="true"></i>
                                        </button>
                                    </div>
                                    <div class="price">29,99 zł</div>
                                    <a href="#" class="position-relative">
                                        <img src="assets/img/trolley.svg" alt="cart icon">
                                        <span class="badge badge-custom small">+</span>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="card">
                            <img src="assets/img/book3.png" class="card-img" alt="...">
                            <div class="card-img-overlay">
                                <div class="row">
                                    <div class="col-4">
                                        <a href="#" class="hearth">
                                            <i class="fa fa-heart-o" aria-hidden="true"></i>
                                            <span>
                                Do schowka
                            </span>
                                        </a>

                                    </div>
                                    <div class="col-8 text-right">
                                        <div class="badge badge-success">Nowość</div>
                                        <div class="badge badge-danger">Promocja</div>
                                        <div class="badge badge-warning">Przedsprzedaż</div>
                                        <div class="badge badge-info">Bestseller</div>
                                    </div>
                                </div>
                            </div>
                            <div class="card-body">
                                <p class="card-text">Mayne Andrew</p>
                                <h5 class="card-title">Naturalista</h5>
                                <div class="card-footer">
                                    <div class="projector_number" id="projector_number_cont">
                                        <button id="projector_number_down" class="projector_number_down" type="button">
                                            <i class="fa fa-minus" aria-hidden="true"></i>
                                        </button>
                                        <input class="projector_number" name="number" id="projector_number" value="1" readonly="readonly">
                                        <button id="projector_number_up" class="projector_number_up" type="button">
                                            <i class="fa fa-plus" aria-hidden="true"></i>
                                        </button>
                                    </div>
                                    <div class="price">29,99 zł</div>
                                    <a href="#" class="position-relative">
                                        <img src="assets/img/trolley.svg" alt="cart icon">
                                        <span class="badge badge-custom small">+</span>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="card">
                            <img src="assets/img/book3.png" class="card-img" alt="...">
                            <div class="card-img-overlay">
                                <div class="row">
                                    <div class="col-4">
                                        <a href="#" class="hearth">
                                            <i class="fa fa-heart-o" aria-hidden="true"></i>
                                            <span>
                                Do schowka
                            </span>
                                        </a>

                                    </div>
                                    <div class="col-8 text-right">
                                        <div class="badge badge-success">Nowość</div>
                                        <div class="badge badge-danger">Promocja</div>
                                        <div class="badge badge-warning">Przedsprzedaż</div>
                                        <div class="badge badge-info">Bestseller</div>
                                    </div>
                                </div>
                            </div>
                            <div class="card-body">
                                <p class="card-text">Mayne Andrew</p>
                                <h5 class="card-title">Naturalista</h5>
                                <div class="card-footer">
                                    <div class="projector_number" id="projector_number_cont">
                                        <button id="projector_number_down" class="projector_number_down" type="button">
                                            <i class="fa fa-minus" aria-hidden="true"></i>
                                        </button>
                                        <input class="projector_number" name="number" id="projector_number" value="1" readonly="readonly">
                                        <button id="projector_number_up" class="projector_number_up" type="button">
                                            <i class="fa fa-plus" aria-hidden="true"></i>
                                        </button>
                                    </div>
                                    <div class="price">29,99 zł</div>
                                    <a href="#" class="position-relative">
                                        <img src="assets/img/trolley.svg" alt="cart icon">
                                        <span class="badge badge-custom small">+</span>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="card card-wide">


                                <div class="row">
                                    <div class="col-md-2">
                                        <img src="assets/img/wide.png" class="card-img" alt="...">


                                    </div>
                                    <div class="col-md-6 text-right">
                                        <div class="card-body">
                                            <p class="card-text">Mayne Andrew</p>
                                            <h5 class="card-title">Naturalista</h5>
                                            <p>W książce przystępnie opisano 80 interesujących przypadków spotykanych w codziennej praktyce anestezjologicznej: od pozornie błahych problemów komunikacji werbalnej, organizacji pracy z agresywnymi współpracownikami, po powikłania różnych technik znieczulenia, ryzyko zakażeń na sali operacyjnej i ciężkie powikłania po operacjach chirurgicznych. </p>

                                        </div>
                                    </div>
                                    <div class="col-md-4 text-right">
                                        <div class="badge badge-danger">Promocja</div>
                                        <div class="badge badge-warning">Przedsprzedaż</div>
                                        <div class="badge badge-info">Bestseller</div>
                                        <a href="#" class="hearth badge">
                                            <i class="fa fa-heart-o" aria-hidden="true"></i>
                                            <span>
                                                Do schowka
                                            </span>
                                        </a>
                                        <div class="card-footer">
                                            <div class="projector_number" id="projector_number_cont">
                                                <button id="projector_number_down" class="projector_number_down" type="button">
                                                    <i class="fa fa-minus" aria-hidden="true"></i>
                                                </button>
                                                <input class="projector_number" name="number" id="projector_number" value="1" readonly="readonly">
                                                <button id="projector_number_up" class="projector_number_up" type="button">
                                                    <i class="fa fa-plus" aria-hidden="true"></i>
                                                </button>
                                            </div>
                                            <div class="price">29,99 zł</div>
                                            <a href="#" class="position-relative">
                                                <img src="assets/img/trolley.svg" alt="cart icon">
                                                <span class="badge badge-custom small">+</span>
                                            </a>
                                        </div>
                                    </div>
                                </div>



                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="card card-wide">


                                <div class="row">
                                    <div class="col-md-2">
                                        <img src="assets/img/top10.png" class="card-img" alt="...">


                                    </div>
                                    <div class="col-md-6 text-right">
                                        <div class="card-body">
                                            <p class="card-text">Mayne Andrew</p>
                                            <h5 class="card-title">Naturalista</h5>
                                            <p>W książce przystępnie opisano 80 interesujących przypadków spotykanych w codziennej praktyce anestezjologicznej: od pozornie błahych problemów komunikacji werbalnej, organizacji pracy z agresywnymi współpracownikami, po powikłania różnych technik znieczulenia, ryzyko zakażeń na sali operacyjnej i ciężkie powikłania po operacjach chirurgicznych. </p>

                                        </div>
                                    </div>
                                    <div class="col-md-4 text-right">
                                        <div class="badge badge-danger">Promocja</div>
                                        <div class="badge badge-warning">Przedsprzedaż</div>
                                        <div class="badge badge-info">Bestseller</div>
                                        <a href="#" class="hearth badge">
                                            <i class="fa fa-heart-o" aria-hidden="true"></i>
                                            <span>
                                                Do schowka
                                            </span>
                                        </a>
                                        <div class="card-footer">
                                            <div class="projector_number" id="projector_number_cont">
                                                <button id="projector_number_down" class="projector_number_down" type="button">
                                                    <i class="fa fa-minus" aria-hidden="true"></i>
                                                </button>
                                                <input class="projector_number" name="number" id="projector_number" value="1" readonly="readonly">
                                                <button id="projector_number_up" class="projector_number_up" type="button">
                                                    <i class="fa fa-plus" aria-hidden="true"></i>
                                                </button>
                                            </div>
                                            <div class="price">29,99 zł</div>
                                            <a href="#" class="position-relative">
                                                <img src="assets/img/trolley.svg" alt="cart icon">
                                                <span class="badge badge-custom small">+</span>
                                            </a>
                                        </div>
                                    </div>
                                </div>



                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php include 'modules/pagination.php' ?>
</div>
<?php
include 'modules/adverts2.php';
include 'modules/comments.php';
include 'modules/footer.php';
