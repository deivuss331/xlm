<?php


include 'modules/header.php';
include 'modules/breadcrumbs.php';?>

<div class="container">
	<div class="row product-box">
		<div class="col-md-3">
			<img src="assets/img/book3.png" class="card-img" alt="...">
			<div class="card-img-overlay text-right">
				<div class="col-6">
					<div class="badge badge-success">Nowość</div>
					<div class="badge badge-danger">Promocja</div>
				</div>
			</div>
		</div>
		<div class="col-md-6">
			<h4>SKU: 541318/M </h4>
			<h1 class="card-title">Neuropsychologia <br>
                medyczna tom 2</h1>

				<!-- <div class="price">29,99 zł</div>
				<a href="#" class="position-relative">
					<img src="assets/img/trolley.svg" alt="cart icon">
					<span class="badge badge-custom small">+</span>
				</a> -->
            <div class="d-flex product-box-review">
				<p>5.3 / 6.0</p>
                <span class="stars"><span class="dis" style="width:60.00%;"></span></span> <p class="product-box-reviews">57 Opinii</p> <a href="#">+ Dodaj Opinie
					<svg xmlns="http://www.w3.org/2000/svg" width="5.974" height="9.984" viewBox="0 0 5.974 9.984">
  						<path d="M1262.011,696.951l3.914-3.951-3.914-3.951,1.031-1.04,4.944,4.992-4.944,4.991Z" transform="translate(-1262.011 -688.008)" fill="#e40000"/>
					</svg>
				</a>
            </div>
            <div class="product-box-available d-flex">
				Dostępność:&nbsp;<span class="green">Dużo > 50 </span> <p>(Wysyłka 3-5 dni roboczych)</p>
				<svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 20 20">
  					<g transform="translate(-1028 -798)">
   						<circle cx="10" cy="10" r="10" transform="translate(1028 798)" fill="#ecf0f1"/>
    					<text transform="translate(1035 813.5)" fill="#30343f" font-size="14" font-family="Roboto-Black, Roboto" font-weight="800" opacity="0.5"><tspan x="0" y="0">?</tspan></text>
  					</g>
				</svg>
            </div>
            <hr>
            <p>W książce starannie omówiono, uporządkowano i usystematyzowano bardzo szeroką i zróżnicowaną wiedzę tworzącą przedmiot neuropsychologii medycznej.<br/>
                <a href="#" >Więcej...</a>
            </p>
            <hr>
            <div class="row product-box-lists">
                <div class="col-sm-6">
                    <ul>
                        <li><strong>Autor:</strong> Carol L. Amstrong, Lisa Morrow</li>
                        <li><strong>Wydawca:</strong> <a href="#" > Wydawnictwo Lekarskie PZWL</a> </li>
                        <li><strong>Rodzaj okładki:</strong>  Miękka</li>
                        <li><strong>Data premiery:</strong> 27/08/2014</li>
                    </ul>
                </div>
                <div class="col-sm-6">
                    <ul>
                        <li><strong>Liczba stron:</strong> 412 stron
                        </li>
                        <li><strong>Waga:</strong> 0,350 kg </li>
                        <li><strong>Wymiary:</strong>  16.5 x 23.5 cm
                        </li>
                        <li><strong>EAN:</strong> 9788320047226</li>
                    </ul>
                </div>
            </div>
		</div>
		<div class="col-md-3">
            <div class="price-box">
                <div class="price-box-link">
					<a href="#" class="hearth active">
                    	<i class="fa fa-heart-o" aria-hidden="true"></i>
                    	<span>Dodaj do ulubionych</span>
                	</a>
				</div>
                <div class="price"><h1>125,10 zł</h1></div>
                <div class="price-box-vat">w tym 23% VAT: 9,16 zł</div>
				<div class="price-box-save">Oszczędzasz 12,46 PLN <span class="green">(-30% rabat)</span></div>
				<div class="price-box-basket d-flex">
					<div class="price-box-basket-amount d-flex">
						<button><p>-</p></button>
						<p>1</p>
						<button><p>+</p></button>
					</div>
					<div class="price-box-basket-add d-flex">
						<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="30.167" height="23.799" viewBox="0 0 30.167 23.799">
  							<defs>
    							<clipPath id="clip-path">
      								<rect width="22.045" height="23.799" fill="#fff"/>
    							</clipPath>
  							</defs>
  							<g clip-path="url(#clip-path)">
    							<path d="M245.279,416.667h13.064a2.7,2.7,0,0,0,2.69-2.69v-5.555a.064.064,0,0,0,0-.029c0-.01-.005-.024-.005-.034s0-.02,0-.029a.271.271,0,0,1-.01-.034c-.005-.01-.005-.019-.01-.029s0-.019-.01-.029a.244.244,0,0,1-.015-.034.113.113,0,0,0-.015-.024l-.014-.029c0-.01-.01-.014-.015-.024s-.015-.02-.02-.029a.082.082,0,0,0-.019-.024c0-.01-.014-.015-.02-.025s-.015-.014-.02-.024-.014-.014-.019-.019l-.024-.024c-.01,0-.015-.015-.024-.019s-.019-.015-.029-.019l-.025-.015a.094.094,0,0,1-.029-.019l-.059-.029-.029-.015c-.01,0-.019,0-.029-.01a.092.092,0,0,0-.034-.01.224.224,0,0,1-.025,0,.1.1,0,0,0-.039-.005s-.01,0-.02,0l-16.558-2.285v-2.31a.308.308,0,0,0,0-.068.035.035,0,0,0,0-.019c0-.015,0-.029,0-.044s0-.024-.01-.039,0-.015,0-.024l-.015-.044c0-.005,0-.015,0-.019a.147.147,0,0,0-.02-.039c0-.005,0-.015-.01-.019a.1.1,0,0,0-.02-.029c0-.01-.01-.015-.015-.024s-.01-.015-.014-.025-.015-.019-.019-.029l-.044-.044-.015-.015a.217.217,0,0,0-.034-.029c0-.005-.015-.01-.02-.015s-.019-.015-.029-.024a.407.407,0,0,0-.039-.024c0-.005-.01-.005-.014-.01l-.063-.029-3.591-1.511a.658.658,0,1,0-.512,1.213l3.187,1.345V418.08a2.693,2.693,0,0,0,2.344,2.665,2.655,2.655,0,1,0,4.931,1.365,2.605,2.605,0,0,0-.365-1.34h5.916a2.619,2.619,0,0,0-.365,1.34,2.656,2.656,0,1,0,2.656-2.656H245.279a1.376,1.376,0,0,1-1.374-1.374V416.3a2.72,2.72,0,0,0,1.374.37Zm3.275,5.438a1.34,1.34,0,1,1-1.34-1.34,1.342,1.342,0,0,1,1.34,1.34Zm10.5,0a1.34,1.34,0,1,1-1.34-1.34,1.342,1.342,0,0,1,1.34,1.34Zm-.707-6.754H245.279a1.376,1.376,0,0,1-1.374-1.374v-7.11l15.813,2.178v4.926a1.377,1.377,0,0,1-1.374,1.379Zm0,0" transform="translate(-239 -401.025)" fill="#fff"/>
  							</g>
  							<circle cx="6.962" cy="6.962" r="6.962" transform="translate(16.244 0.037)" fill="#fff"/>
  							<path d="M4.231-4.772H6.291v1.745H4.231V-.665h-1.9V-3.026H.264V-4.772H2.329V-7.066h1.9Z" transform="translate(20.073 10.895)" fill="#e40000"/>
						</svg>
						<span>DO KOSZYKA</span>
					</div>
				</div>
				<div class="price-box-delivery">
                    <img src="assets/img/shipping-icon.svg" />
                    <p>Do <span>darmowej dostawy</span>&nbsp; brakuje Tobie jeszcze <span>97.00 zł</span></p>
                </div>
			</div>
			<div class="price-box-contact d-flex">
				<div class="price-box-contact-text">
					<div class="price-box-contact-text-first">Potrzebujesz pomocy?</div>
					<div class="price-box-contact-text-second">Pon -  Pt. 9:00 - 17:00</div>
					<div class="price-box-contact-text-third">
						<a href="#">Kontakt
							<svg xmlns="http://www.w3.org/2000/svg" width="5.974" height="9.984" viewBox="0 0 5.974 9.984">
  								<path d="M1262.011,696.951l3.914-3.951-3.914-3.951,1.031-1.04,4.944,4.992-4.944,4.991Z" transform="translate(-1262.011 -688.008)" fill="#e40000"/>
							</svg>
						</a>
					</div>
				</div>
				<div class="price-box-contact-icon">
					<img src="assets/img/contact-icon.svg" />
				</div>
			</div>
		</div>
        <div class="col-3">
            <div class="social product-box-social d-flex">
                <a class="text-center d-xl-flex justify-content-xl-center align-items-xl-center" href="#">
                    <img src="assets/img/facebook.svg">
                </a>
                <a class="text-center d-xl-flex justify-content-xl-center align-items-xl-center" href="#">
                    <img src="assets/img/instagram.svg">
                </a>
                <a class="d-xl-flex justify-content-xl-center align-items-xl-center" href="#">
                    <img src="assets/img/twitter%20(2).svg">
                </a>
            </div>
        </div>
	</div>
</div>

<?php include 'modules/tabs.php';
?>

	<div class="container my-4">
		<div class="row sliderHeader border-top border-bottom">
			<div class="col-lg-6 d-flex justify-content-between border-right">
				<h3><span class="blue">Bestseller </span><strong> &nbsp; z kategorii: &nbsp; </strong> <span class="font-weight-light" id="nowosci"> Upominki i inne</span></h3>
				<div class="dropdown">
					<button class="dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
						Zmień
					</button>
					<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
						<a class="dropdown-item" href="#">Action</a>
						<a class="dropdown-item" href="#">Another action</a>
						<a class="dropdown-item" href="#">Something else here</a>
					</div>
				</div>
			</div>
			<div class="col-lg-6 d-flex justify-content-between">
				<h5>
					Zakup 2 albumy CD z przedsprzedaży a jeden otrzymasz za 50% ceny
				</h5>
				<a href="">Zobacz <i class="fa fa-angle-right"></i></a>
			</div>
		</div>
		<div class="book-slider book-slider2">
			<div class="card">
				<img src="assets/img/book3.png" class="card-img" alt="...">
				<div class="card-img-overlay">
					<div class="row">
						<div class="col-4">
							<a href="#" class="hearth">
								<i class="fa fa-heart-o" aria-hidden="true"></i>
								<span>
                            Do schowka
                        </span>
							</a>

						</div>
						<div class="col-8 text-right">
							<div class="badge badge-success">Nowość</div>
							<div class="badge badge-danger">Promocja</div>
							<div class="badge badge-warning">Przedsprzedaż</div>
							<div class="badge badge-info">Bestseller</div>
						</div>
					</div>
				</div>
				<div class="card-body">
					<p class="card-text">Mayne Andrew</p>
					<h5 class="card-title">Naturalista</h5>
					<div class="card-footer">
						<div class="price">29,99 zł</div>
						<a href="#" class="position-relative">
							<img src="assets/img/trolley.svg" alt="cart icon">
							<span class="badge badge-custom small">+</span>
						</a>
					</div>
				</div>
			</div>
			<div class="card">
				<img src="assets/img/book.png" class="card-img" alt="...">
				<div class="card-img-overlay">
					<div class="row">
						<div class="col-4">
							<a href="#" class="hearth">
								<i class="fa fa-heart-o" aria-hidden="true"></i>
								<span>
                            Do schowka
                        </span>
							</a>

						</div>
						<div class="col-8 text-right">
							<div class="badge badge-success">Nowość</div>
							<div class="badge badge-danger">Promocja</div>
							<div class="badge badge-warning">Przedsprzedaż</div>
							<div class="badge badge-info">Bestseller</div>
						</div>
					</div>
				</div>
				<div class="card-body">
					<p class="card-text">Mayne Andrew</p>
					<h5 class="card-title">Naturalista</h5>
					<div class="card-footer">
						<div class="price">29,99 zł</div>
						<a href="#" class="position-relative">
							<img src="assets/img/trolley.svg" alt="cart icon">
							<span class="badge badge-custom small">+</span>
						</a>
					</div>
				</div>
			</div>
			<div class="card">
				<img src="assets/img/book1.png" class="card-img" alt="...">
				<div class="card-img-overlay">
					<div class="row">
						<div class="col-4">
							<a href="#" class="hearth">
								<i class="fa fa-heart-o" aria-hidden="true"></i>
								<span>
                            Do schowka
                        </span>
							</a>

						</div>
						<div class="col-8 text-right">
							<div class="badge badge-success">Nowość</div>
							<div class="badge badge-danger">Promocja</div>
							<div class="badge badge-warning">Przedsprzedaż</div>
							<div class="badge badge-info">Bestseller</div>
						</div>
					</div>
				</div>
				<div class="card-body">
					<p class="card-text">Mayne Andrew</p>
					<h5 class="card-title">Naturalista</h5>
					<div class="card-footer">
						<div class="price">29,99 zł</div>
						<a href="#" class="position-relative">
							<img src="assets/img/trolley.svg" alt="cart icon">
							<span class="badge badge-custom small">+</span>
						</a>
					</div>
				</div>
			</div>
			<div class="card">
				<img src="assets/img/book2.png" class="card-img" alt="...">
				<div class="card-img-overlay">
					<div class="row">
						<div class="col-4">
							<a href="#" class="hearth active">
								<i class="fa fa-heart-o" aria-hidden="true"></i>
								<span>
                            Do schowka
                        </span>
							</a>

						</div>
						<div class="col-8 text-right">
							<div class="badge badge-success">Nowość</div>
							<div class="badge badge-danger">Promocja</div>
						</div>
					</div>
				</div>
				<div class="card-body">
					<p class="card-text">Mayne Andrew</p>
					<h5 class="card-title">Naturalista</h5>
					<div class="card-footer">
						<div class="price">29,99 zł</div>
						<a href="#" class="position-relative">
							<img src="assets/img/trolley.svg" alt="cart icon">
							<span class="badge badge-custom small">+</span>
						</a>
					</div>
				</div>
			</div>
			<div class="card">
				<img src="assets/img/book3.png" class="card-img" alt="...">
				<div class="card-img-overlay">
					<div class="row">
						<div class="col-4">
							<a href="#" class="hearth active">
								<i class="fa fa-heart-o" aria-hidden="true"></i>
								<span>
                            Do schowka
                        </span>
							</a>

						</div>
						<div class="col-8 text-right">
							<div class="badge badge-success">Nowość</div>
							<div class="badge badge-danger">Promocja</div>
						</div>
					</div>
				</div>
				<div class="card-body">
					<p class="card-text">Mayne Andrew</p>
					<h5 class="card-title">Naturalista</h5>
					<div class="card-footer">
						<div class="price">29,99 zł</div>
						<a href="#" class="position-relative">
							<img src="assets/img/trolley.svg" alt="cart icon">
							<span class="badge badge-custom small">+</span>
						</a>
					</div>
				</div>
			</div>
			<div class="card">
				<img src="assets/img/book4.png" class="card-img" alt="...">
				<div class="card-img-overlay">
					<div class="row">
						<div class="col-4">
							<a href="#" class="hearth active">
								<i class="fa fa-heart-o" aria-hidden="true"></i>
								<span>
                            Do schowka
                        </span>
							</a>

						</div>
						<div class="col-8 text-right">
							<div class="badge badge-success">Nowość</div>
							<div class="badge badge-danger">Promocja</div>
						</div>
					</div>
				</div>
				<div class="card-body">
					<p class="card-text">Mayne Andrew</p>
					<h5 class="card-title">Naturalista</h5>
					<div class="card-footer">
						<div class="price">29,99 zł</div>
						<a href="#" class="position-relative">
							<img src="assets/img/trolley.svg" alt="cart icon">
							<span class="badge badge-custom small">+</span>
						</a>
					</div>
				</div>
			</div>
		</div>
		<div class="arrowSection">
			<div class="row ">
				<div class="offset-lg-4 col-lg-4 slickArrows slickArrows2 ">
				</div>
				<div class="col-lg-4">
					<a href="#" class="button">Pokaż wszystkie</a>
				</div>
			</div>
		</div>
	</div>
<?php
include 'modules/adverts2.php';
include 'modules/comments.php';
include 'modules/footer.php';
