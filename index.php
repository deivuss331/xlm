
<?php

include 'modules/header.php';
include 'modules/hero.php';?>
<div class="container my-4">
	<div class="row sliderHeader border-top border-bottom">
		<div class="col-lg-5 d-flex justify-content-between border-right display-on-rows">
			<h3><span class="green">Nowości </span><strong> &nbsp; z kategorii:</strong></h3>
			<div class="d-flex display-on-rows-first">
				<h3><span class="font-weight-light" id="nowosci"> Upominki i inne</span></h3>
				<div class="dropdown">
					<button class="dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
						Zmień
					</button>
					<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
						<a class="dropdown-item" href="#">Action</a>
						<a class="dropdown-item" href="#">Another action</a>
						<a class="dropdown-item" href="#">Something else here</a>
					</div>
				</div>
			</div>
		</div>
		<div class="col-lg-7 d-flex justify-content-between display-on-rows">
			<h5>
				Zakup 2 albumy CD z przedsprzedaży a jeden otrzymasz za 50% ceny!!
			</h5>
			<a href="">Zobacz <i class="fa fa-angle-right"></i></a>
		</div>
	</div>
	<div class="book-slider book-slider0">
		<div class="card">
			<img src="assets/img/book3.png" class="card-img" alt="...">
			<div class="card-img-overlay">
				<div class="row">
					<div class="col-4">
						<a href="#" class="hearth">
							<i class="fa fa-heart-o" aria-hidden="true"></i>
							<span>
                            Do schowka
                        </span>
						</a>

					</div>
					<div class="col-8 text-right">
						<div class="badge badge-success">Nowość</div>
						<div class="badge badge-danger">Promocja</div>
						<div class="badge badge-warning">Przedsprzedaż</div>
						<div class="badge badge-info">Bestseller</div>
					</div>
				</div>
			</div>
			<div class="card-body">
				<p class="card-text">Mayne Andrew</p>
				<h5 class="card-title">Naturalista</h5>
				<div class="card-footer">
					<div class="display-flex">
						<div class="old-price">32,00zł</div>
						<div class="price">29,99 zł</div>
					</div>
					<a href="#" class="position-relative">
						<img src="assets/img/trolley.svg" alt="cart icon">
						<span class="badge badge-custom small">+</span>
					</a>
				</div>
			</div>
		</div>
		<div class="card">
			<img src="assets/img/book.png" class="card-img" alt="...">
			<div class="card-img-overlay">
				<div class="row">
					<div class="col-4">
						<a href="#" class="hearth">
							<i class="fa fa-heart-o" aria-hidden="true"></i>
							<span>
                            Do schowka
                        </span>
						</a>

					</div>
					<div class="col-8 text-right">
						<div class="badge badge-success">Nowość</div>
						<div class="badge badge-danger">Promocja</div>
						<div class="badge badge-warning">Przedsprzedaż</div>
						<div class="badge badge-info">Bestseller</div>
					</div>
				</div>
			</div>
			<div class="card-body">
				<p class="card-text">Mayne Andrew</p>
				<h5 class="card-title">Naturalista</h5>
				<div class="card-footer">
					<div class="display-flex">
						<div class="old-price">32,00zł</div>
						<div class="price">29,99 zł</div>
					</div>
					<a href="#" class="position-relative">
						<img src="assets/img/trolley.svg" alt="cart icon">
						<span class="badge badge-custom small">+</span>
					</a>
				</div>
			</div>
		</div>
		<div class="card">
			<img src="assets/img/book1.png" class="card-img" alt="...">
			<div class="card-img-overlay">
				<div class="row">
					<div class="col-4">
						<a href="#" class="hearth">
							<i class="fa fa-heart-o" aria-hidden="true"></i>
							<span>
                            Do schowka
                        </span>
						</a>

					</div>
					<div class="col-8 text-right">
						<div class="badge badge-success">Nowość</div>
						<div class="badge badge-danger">Promocja</div>
						<div class="badge badge-warning">Przedsprzedaż</div>
						<div class="badge badge-info">Bestseller</div>
					</div>
				</div>
			</div>
			<div class="card-body">
				<p class="card-text">Mayne Andrew</p>
				<h5 class="card-title">Naturalista</h5>
				<div class="card-footer">
					<div class="display-flex">
						<div class="old-price">32,00zł</div>
						<div class="price">29,99 zł</div>
					</div>
					<a href="#" class="position-relative">
						<img src="assets/img/trolley.svg" alt="cart icon">
						<span class="badge badge-custom small">+</span>
					</a>
				</div>
			</div>
		</div>
		<div class="card">
			<img src="assets/img/book2.png" class="card-img" alt="...">
			<div class="card-img-overlay">
				<div class="row">
					<div class="col-4">
						<a href="#" class="hearth active">
							<i class="fa fa-heart-o" aria-hidden="true"></i>
							<span>
                            Do schowka
                        </span>
						</a>

					</div>
					<div class="col-8 text-right">
						<div class="badge badge-success">Nowość</div>
						<div class="badge badge-danger">Promocja</div>
					</div>
				</div>
			</div>
			<div class="card-body">
				<p class="card-text">Mayne Andrew</p>
				<h5 class="card-title">Naturalista</h5>
				<div class="card-footer">
					<div class="display-flex">
						<div class="old-price">32,00zł</div>
						<div class="price">29,99 zł</div>
					</div>
					<div class="price">29,99 zł</div>
					<a href="#" class="position-relative">
						<img src="assets/img/trolley.svg" alt="cart icon">
						<span class="badge badge-custom small">+</span>
					</a>
				</div>
			</div>
		</div>
		<div class="card">
			<img src="assets/img/book3.png" class="card-img" alt="...">
			<div class="card-img-overlay">
				<div class="row">
					<div class="col-4">
						<a href="#" class="hearth active">
							<i class="fa fa-heart-o" aria-hidden="true"></i>
							<span>
                            Do schowka
                        </span>
						</a>

					</div>
					<div class="col-8 text-right">
						<div class="badge badge-success">Nowość</div>
						<div class="badge badge-danger">Promocja</div>
					</div>
				</div>
			</div>
			<div class="card-body">
				<p class="card-text">Mayne Andrew</p>
				<h5 class="card-title">Naturalista</h5>
				<div class="card-footer">
					<div class="display-flex">
						<div class="old-price">32,00zł</div>
						<div class="price">29,99 zł</div>
					</div>
					<a href="#" class="position-relative">
						<img src="assets/img/trolley.svg" alt="cart icon">
						<span class="badge badge-custom small">+</span>
					</a>
				</div>
			</div>
		</div>
		<div class="card">
			<img src="assets/img/book4.png" class="card-img" alt="...">
			<div class="card-img-overlay">
				<div class="row">
					<div class="col-4">
						<a href="#" class="hearth active">
							<i class="fa fa-heart-o" aria-hidden="true"></i>
							<span>
                            Do schowka
                        </span>
						</a>

					</div>
					<div class="col-8 text-right">
						<div class="badge badge-success">Nowość</div>
						<div class="badge badge-danger">Promocja</div>
					</div>
				</div>
			</div>
			<div class="card-body">
				<p class="card-text">Mayne Andrew</p>
				<h5 class="card-title">Naturalista</h5>
				<div class="card-footer">
					<div class="display-flex">
						<div class="old-price">32,00zł</div>
						<div class="price">29,99 zł</div>
					</div>
					<a href="#" class="position-relative">
						<img src="assets/img/trolley.svg" alt="cart icon">
						<span class="badge badge-custom small">+</span>
					</a>
				</div>
			</div>
		</div>
	</div>
	<div class="arrowSection">
		<div class="row ">
			<div class="offset-lg-4 col-lg-4 slickArrows slickArrows0">
			</div>
			<div class="col-lg-3 see-all-button">
				<a href="#" class="button">Pokaż wszystkie</a>
			</div>
		</div>
	</div>
</div>
<?php
include 'modules/newsletter.php';
?>
<div class="container my-4">
	<div class="row sliderHeader border-top border-bottom">
		<div class="col-lg-6 d-flex justify-content-between border-right display-on-rows">
			<h3><span class="red">Promocje </span><strong> &nbsp; z kategorii:</strong></h3>
			<div class="d-flex display-on-rows-first">
				<h3><span class="font-weight-light" id="nowosci"> Upominki i inne</span></h3>
				<div class="dropdown">
					<button class="dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
						Zmień
					</button>
					<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
						<a class="dropdown-item" href="#">Action</a>
						<a class="dropdown-item" href="#">Another action</a>
						<a class="dropdown-item" href="#">Something else here</a>
					</div>
				</div>
			</div>
		</div>
		<div class="col-lg-6 d-flex justify-content-between display-on-rows">
			<h5>
				Zakup 2 albumy CD z przedsprzedaży a jeden otrzymasz za 50% ceny
			</h5>
			<a href="">Zobacz <i class="fa fa-angle-right"></i></a>
		</div>
	</div>
	<div class="book-slider book-slider1">
		<div class="card">
			<img src="assets/img/book3.png" class="card-img" alt="...">
			<div class="card-img-overlay">
				<div class="row">
					<div class="col-4">
						<a href="#" class="hearth">
							<i class="fa fa-heart-o" aria-hidden="true"></i>
							<span>
                            Do schowka
                        </span>
						</a>

					</div>
					<div class="col-8 text-right">
						<div class="badge badge-success">Nowość</div>
						<div class="badge badge-danger">Promocja</div>
						<div class="badge badge-warning">Przedsprzedaż</div>
						<div class="badge badge-info">Bestseller</div>
					</div>
				</div>
			</div>
			<div class="card-body">
				<p class="card-text">Mayne Andrew</p>
				<h5 class="card-title">Naturalista</h5>
				<div class="card-footer">
					<div class="display-flex">
						<div class="old-price">32,00zł</div>
						<div class="price">29,99 zł</div>
					</div>
					<a href="#" class="position-relative">
						<img src="assets/img/trolley.svg" alt="cart icon">
						<span class="badge badge-custom small">+</span>
					</a>
				</div>
			</div>
		</div>
		<div class="card">
			<img src="assets/img/book.png" class="card-img" alt="...">
			<div class="card-img-overlay">
				<div class="row">
					<div class="col-4">
						<a href="#" class="hearth">
							<i class="fa fa-heart-o" aria-hidden="true"></i>
							<span>
                            Do schowka
                        </span>
						</a>

					</div>
					<div class="col-8 text-right">
						<div class="badge badge-success">Nowość</div>
						<div class="badge badge-danger">Promocja</div>
						<div class="badge badge-warning">Przedsprzedaż</div>
						<div class="badge badge-info">Bestseller</div>
					</div>
				</div>
			</div>
			<div class="card-body">
				<p class="card-text">Mayne Andrew</p>
				<h5 class="card-title">Naturalista</h5>
				<div class="card-footer">
					<div class="display-flex">
						<div class="old-price">32,00zł</div>
						<div class="price">29,99 zł</div>
					</div>
					<a href="#" class="position-relative">
						<img src="assets/img/trolley.svg" alt="cart icon">
						<span class="badge badge-custom small">+</span>
					</a>
				</div>
			</div>
		</div>
		<div class="card">
			<img src="assets/img/book1.png" class="card-img" alt="...">
			<div class="card-img-overlay">
				<div class="row">
					<div class="col-4">
						<a href="#" class="hearth">
							<i class="fa fa-heart-o" aria-hidden="true"></i>
							<span>
                            Do schowka
                        </span>
						</a>

					</div>
					<div class="col-8 text-right">
						<div class="badge badge-success">Nowość</div>
						<div class="badge badge-danger">Promocja</div>
						<div class="badge badge-warning">Przedsprzedaż</div>
						<div class="badge badge-info">Bestseller</div>
					</div>
				</div>
			</div>
			<div class="card-body">
				<p class="card-text">Mayne Andrew</p>
				<h5 class="card-title">Naturalista</h5>
				<div class="card-footer">
					<div class="display-flex">
						<div class="old-price">32,00zł</div>
						<div class="price">29,99 zł</div>
					</div>
					<a href="#" class="position-relative">
						<img src="assets/img/trolley.svg" alt="cart icon">
						<span class="badge badge-custom small">+</span>
					</a>
				</div>
			</div>
		</div>
		<div class="card">
			<img src="assets/img/book2.png" class="card-img" alt="...">
			<div class="card-img-overlay">
				<div class="row">
					<div class="col-4">
						<a href="#" class="hearth active">
							<i class="fa fa-heart-o" aria-hidden="true"></i>
							<span>
                            Do schowka
                        </span>
						</a>

					</div>
					<div class="col-8 text-right">
						<div class="badge badge-success">Nowość</div>
						<div class="badge badge-danger">Promocja</div>
					</div>
				</div>
			</div>
			<div class="card-body">
				<p class="card-text">Mayne Andrew</p>
				<h5 class="card-title">Naturalista</h5>
				<div class="card-footer">
					<div class="display-flex">
						<div class="old-price">32,00zł</div>
						<div class="price">29,99 zł</div>
					</div>
					<a href="#" class="position-relative">
						<img src="assets/img/trolley.svg" alt="cart icon">
						<span class="badge badge-custom small">+</span>
					</a>
				</div>
			</div>
		</div>
		<div class="card">
			<img src="assets/img/book3.png" class="card-img" alt="...">
			<div class="card-img-overlay">
				<div class="row">
					<div class="col-4">
						<a href="#" class="hearth active">
							<i class="fa fa-heart-o" aria-hidden="true"></i>
							<span>
                            Do schowka
                        </span>
						</a>

					</div>
					<div class="col-8 text-right">
						<div class="badge badge-success">Nowość</div>
						<div class="badge badge-danger">Promocja</div>
					</div>
				</div>
			</div>
			<div class="card-body">
				<p class="card-text">Mayne Andrew</p>
				<h5 class="card-title">Naturalista</h5>
				<div class="card-footer">
					<div class="display-flex">
						<div class="old-price">32,00zł</div>
						<div class="price">29,99 zł</div>
					</div>
					<a href="#" class="position-relative">
						<img src="assets/img/trolley.svg" alt="cart icon">
						<span class="badge badge-custom small">+</span>
					</a>
				</div>
			</div>
		</div>
		<div class="card">
			<img src="assets/img/book4.png" class="card-img" alt="...">
			<div class="card-img-overlay">
				<div class="row">
					<div class="col-4">
						<a href="#" class="hearth active">
							<i class="fa fa-heart-o" aria-hidden="true"></i>
							<span>
                            Do schowka
                        </span>
						</a>

					</div>
					<div class="col-8 text-right">
						<div class="badge badge-success">Nowość</div>
						<div class="badge badge-danger">Promocja</div>
					</div>
				</div>
			</div>
			<div class="card-body">
				<p class="card-text">Mayne Andrew</p>
				<h5 class="card-title">Naturalista</h5>
				<div class="card-footer">
					<div class="display-flex">
						<div class="old-price">32,00zł</div>
						<div class="price">29,99 zł</div>
					</div>
					<a href="#" class="position-relative">
						<img src="assets/img/trolley.svg" alt="cart icon">
						<span class="badge badge-custom small">+</span>
					</a>
				</div>
			</div>
		</div>
	</div>
	<div class="arrowSection">
		<div class="row ">
			<div class="offset-lg-4 col-lg-4 slickArrows slickArrows1">
			</div>
			<div class="col-lg-3 see-all-button">
				<a href="#" class="button">Pokaż wszystkie</a>
			</div>
		</div>
	</div>
</div>
<?php
include 'modules/adverts1.php';
?>
	<div class="container my-4">
		<div class="row sliderHeader border-top border-bottom">
			<div class="col-lg-6 d-flex justify-content-between border-right display-on-rows">
				<h3><span class="blue">Bestseller </span><strong> &nbsp; z kategorii:</strong></h3>
				<div class="d-flex display-on-rows-first">
					<h3><span class="font-weight-light" id="nowosci"> Upominki i inne</span></h3>
					<div class="dropdown">
						<button class="dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
							Zmień
						</button>
						<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
							<a class="dropdown-item" href="#">Action</a>
							<a class="dropdown-item" href="#">Another action</a>
							<a class="dropdown-item" href="#">Something else here</a>
						</div>
					</div>
				</div>
			</div>
			<div class="col-lg-6 d-flex justify-content-between display-on-rows">
				<h5>
					Zakup 2 albumy CD z przedsprzedaży a jeden otrzymasz za 50% ceny
				</h5>
				<a href="">Zobacz <i class="fa fa-angle-right"></i></a>
			</div>
		</div>
		<div class="book-slider book-slider2">
			<div class="card">
				<img src="assets/img/book3.png" class="card-img" alt="...">
				<div class="card-img-overlay">
					<div class="row">
						<div class="col-4">
							<a href="#" class="hearth">
								<i class="fa fa-heart-o" aria-hidden="true"></i>
								<span>
                            Do schowka
                        </span>
							</a>

						</div>
						<div class="col-8 text-right">
							<div class="badge badge-success">Nowość</div>
							<div class="badge badge-danger">Promocja</div>
							<div class="badge badge-warning">Przedsprzedaż</div>
							<div class="badge badge-info">Bestseller</div>
						</div>
					</div>
				</div>
				<div class="card-body">
					<p class="card-text">Mayne Andrew</p>
					<h5 class="card-title">Naturalista</h5>
					<div class="card-footer">
						<div class="display-flex">
							<div class="old-price">32,00zł</div>
							<div class="price">29,99 zł</div>
						</div>
						<a href="#" class="position-relative">
							<img src="assets/img/trolley.svg" alt="cart icon">
							<span class="badge badge-custom small">+</span>
						</a>
					</div>
				</div>
			</div>
			<div class="card">
				<img src="assets/img/book.png" class="card-img" alt="...">
				<div class="card-img-overlay">
					<div class="row">
						<div class="col-4">
							<a href="#" class="hearth">
								<i class="fa fa-heart-o" aria-hidden="true"></i>
								<span>
                            Do schowka
                        </span>
							</a>

						</div>
						<div class="col-8 text-right">
							<div class="badge badge-success">Nowość</div>
							<div class="badge badge-danger">Promocja</div>
							<div class="badge badge-warning">Przedsprzedaż</div>
							<div class="badge badge-info">Bestseller</div>
						</div>
					</div>
				</div>
				<div class="card-body">
					<p class="card-text">Mayne Andrew</p>
					<h5 class="card-title">Naturalista</h5>
					<div class="card-footer">
						<div class="display-flex">
							<div class="old-price">32,00zł</div>
							<div class="price">29,99 zł</div>
						</div>
						<a href="#" class="position-relative">
							<img src="assets/img/trolley.svg" alt="cart icon">
							<span class="badge badge-custom small">+</span>
						</a>
					</div>
				</div>
			</div>
			<div class="card">
				<img src="assets/img/book1.png" class="card-img" alt="...">
				<div class="card-img-overlay">
					<div class="row">
						<div class="col-4">
							<a href="#" class="hearth">
								<i class="fa fa-heart-o" aria-hidden="true"></i>
								<span>
                            Do schowka
                        </span>
							</a>

						</div>
						<div class="col-8 text-right">
							<div class="badge badge-success">Nowość</div>
							<div class="badge badge-danger">Promocja</div>
							<div class="badge badge-warning">Przedsprzedaż</div>
							<div class="badge badge-info">Bestseller</div>
						</div>
					</div>
				</div>
				<div class="card-body">
					<p class="card-text">Mayne Andrew</p>
					<h5 class="card-title">Naturalista</h5>
					<div class="card-footer">
						<div class="display-flex">
							<div class="old-price">32,00zł</div>
							<div class="price">29,99 zł</div>
						</div>
						<a href="#" class="position-relative">
							<img src="assets/img/trolley.svg" alt="cart icon">
							<span class="badge badge-custom small">+</span>
						</a>
					</div>
				</div>
			</div>
			<div class="card">
				<img src="assets/img/book2.png" class="card-img" alt="...">
				<div class="card-img-overlay">
					<div class="row">
						<div class="col-4">
							<a href="#" class="hearth active">
								<i class="fa fa-heart-o" aria-hidden="true"></i>
								<span>
                            Do schowka
                        </span>
							</a>

						</div>
						<div class="col-8 text-right">
							<div class="badge badge-success">Nowość</div>
							<div class="badge badge-danger">Promocja</div>
						</div>
					</div>
				</div>
				<div class="card-body">
					<p class="card-text">Mayne Andrew</p>
					<h5 class="card-title">Naturalista</h5>
					<div class="card-footer">
						<div class="display-flex">
							<div class="old-price">32,00zł</div>
							<div class="price">29,99 zł</div>
						</div>
						<a href="#" class="position-relative">
							<img src="assets/img/trolley.svg" alt="cart icon">
							<span class="badge badge-custom small">+</span>
						</a>
					</div>
				</div>
			</div>
			<div class="card">
				<img src="assets/img/book3.png" class="card-img" alt="...">
				<div class="card-img-overlay">
					<div class="row">
						<div class="col-4">
							<a href="#" class="hearth active">
								<i class="fa fa-heart-o" aria-hidden="true"></i>
								<span>
                            Do schowka
                        </span>
							</a>

						</div>
						<div class="col-8 text-right">
							<div class="badge badge-success">Nowość</div>
							<div class="badge badge-danger">Promocja</div>
						</div>
					</div>
				</div>
				<div class="card-body">
					<p class="card-text">Mayne Andrew</p>
					<h5 class="card-title">Naturalista</h5>
					<div class="card-footer">
						<div class="display-flex">
							<div class="old-price">32,00zł</div>
							<div class="price">29,99 zł</div>
						</div>
						<a href="#" class="position-relative">
							<img src="assets/img/trolley.svg" alt="cart icon">
							<span class="badge badge-custom small">+</span>
						</a>
					</div>
				</div>
			</div>
			<div class="card">
				<img src="assets/img/book4.png" class="card-img" alt="...">
				<div class="card-img-overlay">
					<div class="row">
						<div class="col-4">
							<a href="#" class="hearth active">
								<i class="fa fa-heart-o" aria-hidden="true"></i>
								<span>
                            Do schowka
                        </span>
							</a>

						</div>
						<div class="col-8 text-right">
							<div class="badge badge-success">Nowość</div>
							<div class="badge badge-danger">Promocja</div>
						</div>
					</div>
				</div>
				<div class="card-body">
					<p class="card-text">Mayne Andrew</p>
					<h5 class="card-title">Naturalista</h5>
					<div class="card-footer">
						<div class="display-flex">
							<div class="old-price">32,00zł</div>
							<div class="price">29,99 zł</div>
						</div>
						<a href="#" class="position-relative">
							<img src="assets/img/trolley.svg" alt="cart icon">
							<span class="badge badge-custom small">+</span>
						</a>
					</div>
				</div>
			</div>
		</div>
		<div class="arrowSection">
			<div class="row ">
				<div class="offset-lg-4 col-lg-4 slickArrows slickArrows2 ">
				</div>
				<div class="col-lg-3 see-all-button">
					<a href="#" class="button">Pokaż wszystkie</a>
				</div>
			</div>
		</div>
	</div>
<?php
include 'modules/adverts2.php';
include 'modules/comments.php';
include 'modules/footer.php';