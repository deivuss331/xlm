<footer class="footer-clean">
    <div class="container mb-5">
        <div class="row justify-content-center footer-content">
                <div class="col-md-2 item">
                    <h3>Metody płatności</h3>
                    <ul>
                        <li><a href="#">Za pobraniem</a></li>
                        <li><a href="#">MasterCard</a></li>
                        <li><a href="#">Visa</a></li>
                        <li><a href="#">PayU</a></li>
                        <li><a href="#">PayPal</a></li>
                        <li><a href="#">Blik</a></li>
                    </ul>
                </div>
                <div class="col-md-2 item logistic-partners">
                    <h3>Partnerzy logistyczni</h3>
                    <ul>
                        <li><a href="#">Fedex</a></li>
                        <li><a href="#">InPost</a></li>
                        <li><a href="#">Poczta Polska</a></li>
                    </ul>
                </div>
            <div class="col-md-8 item">
                <h3 class="h-footer" >Najpopularniejsi autorzy</h3>
                <div class="row">
                    <div class="col-md-6 col-lg-3">
                        <ul>
                            <li><a href="#">Wojciech Sumliński</a></li>
                            <li><a href="#">Wiktor Suwrow</a></li>
                            <li><a href="#">Sławiomir Cenckiewicz</a></li>
                            <li><a href="#">Rafał Ziemkiewicz</a></li>
                            <li><a href="#">Dorota Kania</a></li>
                            <li><a href="#">Bronisław Wildstein</a></li>
                            <li><a href="#">Wojciech Coejrowski</a></li>
                            <li><a href="#">Marcin Wolski</a></li>
                            <li><a href="#">Tadeusz Płażański</a></li>
                            <li><a href="#">Marek Jan Chodakiewicz</a></li>
                        </ul>
                    </div>
                    <div class="col-md-6 col-lg-3">
                        <ul>
                            <li><a href="#">Andrzej Nowak</a></li>
                            <li><a href="#">Norman Davies</a></li>
                            <li><a href="#">Bohdan Urbankowski</a></li>
                            <li><a href="#">Detrich Von Hildebrand</a></li>
                            <li><a href="#">Filip Musiał</a></li>
                            <li><a href="#">Józef Mackiewicz</a></li>
                            <li><a href="#">Paweł Wieczorkiewicz</a></li>
                            <li><a href="#">Gilbert Keith Chesterton</a></li>
                            <li><a href="#">Adam Szustak</a></li>
                            <li><a href="#">Clive Staples Lewis</a></li>
                            <li><a href="#">Jacek Pulikowski</a></li>
                        </ul>
                    </div>
                    <div class="col-md-6 col-lg-3">
                        <ul>
                            <li><a href="#">Aleksander Posacki</a></li>
                            <li><a href="#">Waldemar Chrostowski</a></li>
                            <li><a href="#">Nick Vujcic</a></li>
                            <li><a href="#">Richard Rohr</a></li>
                            <li><a href="#">Scott Hahn</a></li>
                            <li><a href="#">Jan Paweł II</a></li>
                            <li><a href="#">George Orwell</a></li>
                            <li><a href="#">Jacek Piekara</a></li>
                            <li><a href="#">Vladimir Volkoff</a></li>
                            <li><a href="#">Waldemar Łysiak</a></li>
                            <li><a href="#">Jean Raspail</a></li>
                        </ul>
                    </div>
                    <div class="col-lg-3">
                        <ul>
                            <li><a href="#">Sergiusz Piasecki</a></li>
                            <li><a href="#">Agatha Christie</a></li>
                            <li><a href="#">J.R.R. Tolkien</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-md-2 item social">
                <h3 class="d-xl-flex justify-content-xl-start align-items-xl-start">Careers</h3>
                <ul>
                    <li><a href="#">Job openings</a></li>
                    <li><a href="#">Employee success</a></li>
                    <li><a href="#">Benefits</a></li>
                </ul>
                <div class="d-flex flex-row flex-grow-1 flex-shrink-1 justify-content-center flex-wrap flex-md-nowrap justify-content-md-end social-icons">
                    <a class="text-center d-xl-flex justify-content-xl-center align-items-xl-center" href="#">
                        <img src="./assets/img/facebook.svg">
                    </a>
                    <a class="text-center d-xl-flex justify-content-xl-center align-items-xl-center" href="#">
                        <img src="./assets/img/instagram.svg">
                    </a>
                    <a class="d-xl-flex justify-content-xl-center align-items-xl-center" href="#">
                        <img src="./assets/img/youtube%20(1).svg">
                    </a>
                    <a class="d-xl-flex justify-content-xl-center align-items-xl-center" href="#">
                        <img src="./assets/img/twitter%20(2).svg">
                    </a>
                </div>
            </div>
        </div>
    </div>
    <div class="copyright">
        <div class="container">
            <p>Copyright. 2019 XLM</p>
            <a href="#"><img src="./assets/img/logo.svg" alt="cart icon"></a>
            <a href="#">MIGOMEDIA</a>
        </div>
    </div>
</footer>
<script src="./assets/js/jquery.min.js"></script>
<script src="./assets/libs/bootstrap/js/bootstrap.min.js"></script>
<script src="./assets/libs/slick/slick.min.js"></script>
<script src="./assets/js/main.js"></script>
</body>
</html>