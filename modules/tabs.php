<?php
/**
 * Created by PhpStorm.
 * User: dariuszkrol
 * Date: 20/01/2020
 * Time: 13:29
 */
?>
<div class="tabs">
	<div class="container">
		<nav>
			<div class="nav nav-tabs" id="nav-tab" role="tablist">
				<a class="col nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab" aria-controls="nav-home" aria-selected="true">Opis</a>
				<a class="col nav-item nav-link" id="nav-profile-tab" data-toggle="tab" href="#nav-profile" role="tab" aria-controls="nav-profile" aria-selected="false">Produkty pokrewne</a>
				<a class="col nav-item nav-link" id="nav-contact-tab" data-toggle="tab" href="#nav-contact" role="tab" aria-controls="nav-contact" aria-selected="false">Recenzje</a>
			</div>
		</nav>
		<div class="tab-content content" id="nav-tabContent">
			<div class="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
				<p>W książce starannie omówiono, uporządkowano i usystematyzowano bardzo szeroką i zróżnicowaną wiedzę tworzącą przedmiot neuropsychologii medycznej.</p>
				<p>Czytelnicy znajdą w publikacji aktualne i nowatorskie wiadomości umożliwiające zrozumienie różnych chorób i zaburzeń oraz ich skutków neuropsychologicznych oraz ważne dane bibliograficzne zachęcające do dalszych poszukiwań.</p>
				<p><br>
					Tom II<br>

						- Problemy dotyczące starzenia: demencja, zaburzenia ruchowe<br>
						- Choroby o podłożu immunologicznym: SM, zespół Guillaina-Barrégo, choroby autoimmunologiczne i reumatyczne<br>
						- Neurologiczne i poznawcze konsekwencje zakażenia wirusem HIV-1,<br>
						- Zespół Sj?grena, fibromialgia i zespół przewlekłego zmęczenia<br>
						- Choroby endokrynologiczne: cukrzyca typu 1 i 2, zaburzenia hormonalne, pourazowe zaburzenia stresowe (PTSD)<br>
						- Choroby metaboliczne: encefalopatia wątrobowa, zaburzenia wywołane przez czynniki toksyczne, choroby mitochondrialne<br>
						- Współczesne podejścia do rehabilitacji poznawczej, zaburzenia sensoryczne<br><br>

				</p>
				<p>	Publikacja zainteresuje z pewnością neuropsychologów, neurologów, psychologów i psychiatrów. Będzie również przydatnym podręcznikiem dla studentów psychologii, pedagogiki, logopedii, neurologopedii oraz medycyny.
				</p>
				<a href="#">Zajrzyj do książki</a>

			</div>
			<div class="tab-pane fade" id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab">

				<div class="book-slider book-slider0">
					<div class="card">
						<img src="./assets/img/book3.png" class="card-img" alt="...">
						<div class="card-img-overlay">
							<div class="row">
								<div class="col-4">
									<a href="#" class="hearth">
										<i class="fa fa-heart-o" aria-hidden="true"></i>
										<span>
	                        Do schowka
	                    </span>
									</a>

								</div>
								<div class="col-8 text-right">
									<div class="badge badge-success">Nowość</div>
									<div class="badge badge-danger">Promocja</div>
									<div class="badge badge-warning">Przedsprzedaż</div>
									<div class="badge badge-info">Bestseller</div>
								</div>
							</div>
						</div>
						<div class="card-body">
							<p class="card-text">Mayne Andrew</p>
							<h5 class="card-title">Naturalista</h5>
							<div class="card-footer">
								<div class="price">29,99 zł</div>
								<a href="#" class="position-relative">
									<img src="./assets/img/trolley.svg" alt="cart icon">
									<span class="badge badge-custom small">+</span>
								</a>
							</div>
						</div>
					</div>
					<div class="card">
						<img src="./assets/img/book.png" class="card-img" alt="...">
						<div class="card-img-overlay">
							<div class="row">
								<div class="col-4">
									<a href="#" class="hearth">
										<i class="fa fa-heart-o" aria-hidden="true"></i>
										<span>
	                        Do schowka
	                    </span>
									</a>

								</div>
								<div class="col-8 text-right">
									<div class="badge badge-success">Nowość</div>
									<div class="badge badge-danger">Promocja</div>
									<div class="badge badge-warning">Przedsprzedaż</div>
									<div class="badge badge-info">Bestseller</div>
								</div>
							</div>
						</div>
						<div class="card-body">
							<p class="card-text">Mayne Andrew</p>
							<h5 class="card-title">Naturalista</h5>
							<div class="card-footer">
								<div class="price">29,99 zł</div>
								<a href="#" class="position-relative">
									<img src="./assets/img/trolley.svg" alt="cart icon">
									<span class="badge badge-custom small">+</span>
								</a>
							</div>
						</div>
					</div>
					<div class="card">
						<img src="./assets/img/book1.png" class="card-img" alt="...">
						<div class="card-img-overlay">
							<div class="row">
								<div class="col-4">
									<a href="#" class="hearth">
										<i class="fa fa-heart-o" aria-hidden="true"></i>
										<span>
	                        Do schowka
	                    </span>
									</a>

								</div>
								<div class="col-8 text-right">
									<div class="badge badge-success">Nowość</div>
									<div class="badge badge-danger">Promocja</div>
									<div class="badge badge-warning">Przedsprzedaż</div>
									<div class="badge badge-info">Bestseller</div>
								</div>
							</div>
						</div>
						<div class="card-body">
							<p class="card-text">Mayne Andrew</p>
							<h5 class="card-title">Naturalista</h5>
							<div class="card-footer">
								<div class="price">29,99 zł</div>
								<a href="#" class="position-relative">
									<img src="./assets/img/trolley.svg" alt="cart icon">
									<span class="badge badge-custom small">+</span>
								</a>
							</div>
						</div>
					</div>
					<div class="card">
						<img src="./assets/img/book2.png" class="card-img" alt="...">
						<div class="card-img-overlay">
							<div class="row">
								<div class="col-4">
									<a href="#" class="hearth active">
										<i class="fa fa-heart-o" aria-hidden="true"></i>
										<span>
	                        Do schowka
	                    </span>
									</a>

								</div>
								<div class="col-8 text-right">
									<div class="badge badge-success">Nowość</div>
									<div class="badge badge-danger">Promocja</div>
								</div>
							</div>
						</div>
						<div class="card-body">
							<p class="card-text">Mayne Andrew</p>
							<h5 class="card-title">Naturalista</h5>
							<div class="card-footer">
								<div class="price">29,99 zł</div>
								<a href="#" class="position-relative">
									<img src="./assets/img/trolley.svg" alt="cart icon">
									<span class="badge badge-custom small">+</span>
								</a>
							</div>
						</div>
					</div>
					<div class="card">
						<img src="./assets/img/book3.png" class="card-img" alt="...">
						<div class="card-img-overlay">
							<div class="row">
								<div class="col-4">
									<a href="#" class="hearth active">
										<i class="fa fa-heart-o" aria-hidden="true"></i>
										<span>
	                        Do schowka
	                    </span>
									</a>

								</div>
								<div class="col-8 text-right">
									<div class="badge badge-success">Nowość</div>
									<div class="badge badge-danger">Promocja</div>
								</div>
							</div>
						</div>
						<div class="card-body">
							<p class="card-text">Mayne Andrew</p>
							<h5 class="card-title">Naturalista</h5>
							<div class="card-footer">
								<div class="price">29,99 zł</div>
								<a href="#" class="position-relative">
									<img src="./assets/img/trolley.svg" alt="cart icon">
									<span class="badge badge-custom small">+</span>
								</a>
							</div>
						</div>
					</div>
					<div class="card">
						<img src="./assets/img/book4.png" class="card-img" alt="...">
						<div class="card-img-overlay">
							<div class="row">
								<div class="col-4">
									<a href="#" class="hearth active">
										<i class="fa fa-heart-o" aria-hidden="true"></i>
										<span>
	                        Do schowka
	                    </span>
									</a>

								</div>
								<div class="col-8 text-right">
									<div class="badge badge-success">Nowość</div>
									<div class="badge badge-danger">Promocja</div>
								</div>
							</div>
						</div>
						<div class="card-body">
							<p class="card-text">Mayne Andrew</p>
							<h5 class="card-title">Naturalista</h5>
							<div class="card-footer">
								<div class="price">29,99 zł</div>
								<a href="#" class="position-relative">
									<img src="./assets/img/trolley.svg" alt="cart icon">
									<span class="badge badge-custom small">+</span>
								</a>
							</div>
						</div>
					</div>
				</div>
				<div class="arrowSection">
					<div class="row ">
						<div class="offset-4 col-lg-4 slickArrows slickArrows0">
						</div>
					</div>
				</div>
			</div>
			<div class="tab-pane fade" id="nav-contact" role="tabpanel" aria-labelledby="nav-contact-tab">
				<div class="comments">
					<div class="slide comment">
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </p>
						<div class="author">Mateusz P.</div>
						<div class="rating">
							5.3 / 6.0
							<span class="stars"><span class="dis" style="width:40.00%;"></span></span>
						</div>
						<div class="opinion float-right">
							<a href="" class="like"><i class="fa fa-thumbs-up" aria-hidden="true"></i>3</a>
							<a href="" class="dislike"><i class="fa fa-thumbs-down" aria-hidden="true"></i>0</a>
						</div>
					</div>
					<div class="slide comment">
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </p>
						<div class="author">Mateusz P.</div>
						<div class="rating">
							5.3 / 6.0
							<span class="stars"><span class="dis" style="width:80.00%;"></span></span>
						</div>
						<div class="opinion float-right">
							<a href="" class="like"><i class="fa fa-thumbs-up" aria-hidden="true"></i>3</a>
							<a href="" class="dislike"><i class="fa fa-thumbs-down" aria-hidden="true"></i>0</a>
						</div>
					</div>
					<div class="slide comment">
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </p>
						<div class="author">Mateusz P.</div>
						<div class="rating">
							5.3 / 6.0
							<span class="stars"><span class="dis" style="width:80.00%;"></span></span>
						</div>
						<div class="opinion float-right">
							<a href="" class="like"><i class="fa fa-thumbs-up" aria-hidden="true"></i>3</a>
							<a href="" class="dislike"><i class="fa fa-thumbs-down" aria-hidden="true"></i>0</a>
						</div>
					</div>
					<div class="slide comment">
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </p>
						<div class="author">Mateusz P.</div>
						<div class="rating">
							5.3 / 6.0
							<span class="stars"><span class="dis" style="width:60.00%;"></span></span>
						</div>
						<div class="opinion float-right">
							<a href="" class="like"><i class="fa fa-thumbs-up" aria-hidden="true"></i>3</a>
							<a href="" class="dislike"><i class="fa fa-thumbs-down" aria-hidden="true"></i>0</a>
						</div>
					</div>

				<form action="" class="mt-4 d-block">
					<h5>Twoja recenzja</h5>
					<div class="form-group">
						<label for="exampleFormControlTextarea1"></label>
						<textarea class="form-control rounded-0" id="exampleFormControlTextarea1" rows="10" placeholder="Treść Twojej recenzji..."></textarea>
					</div>
					<div class="row">
						<div class="col-md-6 d-flex align-items-center">
							<div class="form-group w-75">
								<label for="exampleFormControlTextarea1"></label>
								<input class="form-control rounded-0" id="inputNama" placeholder="Imię i nazwisko lub nick" />
							</div>
							<div class="w-25">
								<span class="stars"><span class="dis" style="width:60.00%;"></span></span>
							</div>
						</div>
						<div class="col-md-6">
							<button type="submit" class="button">Publikuj</button>
						</div>
					</div>


					</form>
				</div>
			</div>
		</div>
	</div>
</div>
