<?php
/**
 * Created by PhpStorm.
 * User: dariuszkrol
 * Date: 08/01/2020
 * Time: 15:06
 */

?>
<div class="container">
    <div class="row">
        <div class="col-md-4">
            <div class="row">
                <div class="col-md-12">
                    <h1>Nagłówek h1</h1>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <h2>Nagłówek h2</h2>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <h3>Nagłówek h3</h3>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <h4>Nagłówek h4</h4>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <h5>Nagłówek h5</h5>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <h6>Nagłówek h6</h6>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="row">
                <div class="col-md-12"><a href="#" class="button">Wszystkie aktualnosci</a></div>
                <div class="col-md-12"><a href="#" class="button whiteButton">Przejdź dalej</a></div>
            </div>
        </div>
        <div class="col-md-4">
            <p> Lorem ipsum dolor sit amet, consectetur
<a href="#">link lokalny</a>
adipisicing elit. Dignissimos distinctio,
                facilis iure maiores quisquam quo?
                <a href="#" target="_blank">Link zewnetrzny</a>
Lorem ipsum dolor sit amet, consectetur adipisicing elit. Expedita harum, id! Aut.
                </a>
            </p>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <ul>
                <li>Element listy</li>
                <li>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Blanditiis nam optio perspiciatis.</li>
                <li>Lorem ipsum dolor</li>
                <li>Lorem ipsum dolor</li>
                <li>Lorem ipsum dolor</li>
                <li>Lorem ipsum dolor</li>
            </ul>
        </div>
        <div class="col-md-6">
            <div class="accordion" id="accordion">
                <div class="card">
                    <div class="card-header" id="headingOne">
                        <h4 class="mb-0">
                            <button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
    Collapsible Group Item
</button>
                        </h4>
                    </div>

                    <div id="collapseOne" class="collapse" aria-labelledby="headingOne" data-parent="#accordion">
                        <div class="card-body">
Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingTwo">
                        <h4 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                Collapsible Group Item
                            </button>
                        </h4>
                    </div>
                    <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
                        <div class="card-body">
                            Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingThree">
                        <h4 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
    Collapsible Group Item
</button>
                        </h4>
                    </div>
                    <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                        <div class="card-body">
Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="container">
    <div class="row">
        <div class="col-md-12">
            <table>
                <thead>
                <tr>
                    <th scope="col">Kolumna A</th>
                    <th scope="col">Kolumna B</th>
                    <th scope="col">Kolumna C</th>
                    <th scope="col">Kolumna D</th>

                </tr>
                </thead>
                <tbody>
                <tr>
                    <td scope="row">1</td>
                    <td>Mark</td>
                    <td>Otto</td>
                    <td>@mdo</td>
                </tr>
                <tr>
                    <td scope="row">2</td>
                    <td>Jacob</td>
                    <td>Thornton</td>
                    <td>@fat</td>
                </tr>
                <tr>
                    <td scope="row">3</td>
                    <td colspan="2">Larry the Bird</td>
                    <td>@twitter</td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>






