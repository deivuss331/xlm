<div class="container">
	<div class="headerSection">
		<div class="row">
			<div class="col-md-6">
				<h1>Medycyna</h1>
			</div>
			<div class="col-md-6 page-settings">
				<a href=""><i class="fa fa-th" aria-hidden="true"></i></a>
				<a href=""><i class="fa fa-th-large" aria-hidden="true"></i></a>
				<a href=""><i class="fa fa-bars" aria-hidden="true"></i></a>
				<div class="with-dropdown">
				Produkty na stronie
					<div class="dropdown">
						<button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
							30 Sztuk
						</button>
						<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
							<a class="dropdown-item" href="#">Action</a>
							<a class="dropdown-item" href="#">Another action</a>
							<a class="dropdown-item" href="#">Something else here</a>
						</div>
					</div>
				</div>
				<div class="with-dropdown">
				Sortuj według
					<div class="dropdown">
						<button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
							Cena od najniższej
						</button>
						<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
							<a class="dropdown-item" href="#">Action</a>
							<a class="dropdown-item" href="#">Another action</a>
							<a class="dropdown-item" href="#">Something else here</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
