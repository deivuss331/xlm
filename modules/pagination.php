<div class="container">
	<div class="pagination-section">
		<nav aria-label="Page navigation example">
			<ul class="pagination">
				<li class="page-item"><a class="page-link" href="#"><i class="fa fa-angle-left" aria-hidden="true"></i></a></li>
				<li class="page-item"><a class="page-link" href="#">1</a></li>
				<li class="page-item current"><a class="page-link" href="#">2</a></li>
				<li class="page-item"><a class="page-link" href="#">3</a></li>
				<li class="page-item"><a class="page-link" href="#">4</a></li>
				<li class="page-item">
					<form action="">
						<input type="text" placeholder="Wpisz np 7">
					</form>
				</li>
				<li class="page-item"><a class="page-link" href="#"><i class="fa fa-angle-right" aria-hidden="true"></i></a></li>
			</ul>
		</nav>
	</div>
</div>
