<div class="container my-4">
    <div class="row sliderHeader border-top border-bottom">
        <div class="col-lg-5 d-flex justify-content-between border-right">
            <h3><span class="green">Nowości </span><strong> &nbsp; z kategorii: &nbsp; </strong> <span class="font-weight-light" id="nowosci"> Upominki i inne</span></h3>
            <div class="dropdown">
                <button class="dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Zmień
                </button>
                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                    <a class="dropdown-item" href="#">Action</a>
                    <a class="dropdown-item" href="#">Another action</a>
                    <a class="dropdown-item" href="#">Something else here</a>
                </div>
            </div>
        </div>
        <div class="col-lg-7 d-flex justify-content-between">
            <h5>
                Zakup 2 albumy CD z przedsprzedaży a jeden otrzymasz za 50% ceny
            </h5>
            <a href="">Zobacz <i class="fa fa-angle-right"></i></a>
        </div>
    </div>
	<div class="book-slider book-slider0">
		<div class="card">
			<img src="./assets/img/book3.png" class="card-img" alt="...">
			<div class="card-img-overlay">
                <div class="row">
                    <div class="col-4">
                        <a href="#" class="hearth">
                            <i class="fa fa-heart-o" aria-hidden="true"></i>
                            <span>
                            Do schowka
                        </span>
                        </a>

                    </div>
                    <div class="col-8 text-right">
                        <div class="badge badge-success">Nowość</div>
                        <div class="badge badge-danger">Promocja</div>
                        <div class="badge badge-warning">Przedsprzedaż</div>
                        <div class="badge badge-info">Bestseller</div>
                    </div>
                </div>
			</div>
			<div class="card-body">
				<p class="card-text">Mayne Andrew</p>
				<h5 class="card-title">Naturalista</h5>
				<div class="card-footer">
					<div class="price">29,99 zł</div>
					<a href="#" class="position-relative">
                        <img src="./assets/img/trolley.svg" alt="cart icon">
                        <span class="badge badge-custom small">+</span>
                    </a>
				</div>
			</div>
		</div>
        <div class="card">
            <img src="./assets/img/book.png" class="card-img" alt="...">
            <div class="card-img-overlay">
                <div class="row">
                    <div class="col-4">
                        <a href="#" class="hearth">
                            <i class="fa fa-heart-o" aria-hidden="true"></i>
                            <span>
                            Do schowka
                        </span>
                        </a>

                    </div>
                    <div class="col-8 text-right">
                        <div class="badge badge-success">Nowość</div>
                        <div class="badge badge-danger">Promocja</div>
                        <div class="badge badge-warning">Przedsprzedaż</div>
                        <div class="badge badge-info">Bestseller</div>
                    </div>
                </div>
            </div>
            <div class="card-body">
                <p class="card-text">Mayne Andrew</p>
                <h5 class="card-title">Naturalista</h5>
                <div class="card-footer">
                    <div class="price">29,99 zł</div>
                    <a href="#" class="position-relative">
                        <img src="./assets/img/trolley.svg" alt="cart icon">
                        <span class="badge badge-custom small">+</span>
                    </a>
                </div>
            </div>
        </div>
        <div class="card">
            <img src="./assets/img/book1.png" class="card-img" alt="...">
            <div class="card-img-overlay">
                <div class="row">
                    <div class="col-4">
                        <a href="#" class="hearth">
                            <i class="fa fa-heart-o" aria-hidden="true"></i>
                            <span>
                            Do schowka
                        </span>
                        </a>

                    </div>
                    <div class="col-8 text-right">
                        <div class="badge badge-success">Nowość</div>
                        <div class="badge badge-danger">Promocja</div>
                        <div class="badge badge-warning">Przedsprzedaż</div>
                        <div class="badge badge-info">Bestseller</div>
                    </div>
                </div>
            </div>
            <div class="card-body">
                <p class="card-text">Mayne Andrew</p>
                <h5 class="card-title">Naturalista</h5>
                <div class="card-footer">
                    <div class="price">29,99 zł</div>
                    <a href="#" class="position-relative">
                        <img src="./assets/img/trolley.svg" alt="cart icon">
                        <span class="badge badge-custom small">+</span>
                    </a>
                </div>
            </div>
        </div>
        <div class="card">
            <img src="./assets/img/book2.png" class="card-img" alt="...">
            <div class="card-img-overlay">
                <div class="row">
                    <div class="col-4">
                        <a href="#" class="hearth active">
                            <i class="fa fa-heart-o" aria-hidden="true"></i>
                            <span>
                            Do schowka
                        </span>
                        </a>

                    </div>
                    <div class="col-8 text-right">
                        <div class="badge badge-success">Nowość</div>
                        <div class="badge badge-danger">Promocja</div>
                    </div>
                </div>
            </div>
            <div class="card-body">
                <p class="card-text">Mayne Andrew</p>
                <h5 class="card-title">Naturalista</h5>
                <div class="card-footer">
                    <div class="price">29,99 zł</div>
                    <a href="#" class="position-relative">
                        <img src="./assets/img/trolley.svg" alt="cart icon">
                        <span class="badge badge-custom small">+</span>
                    </a>
                </div>
            </div>
        </div>
        <div class="card">
            <img src="./assets/img/book3.png" class="card-img" alt="...">
            <div class="card-img-overlay">
                <div class="row">
                    <div class="col-4">
                        <a href="#" class="hearth active">
                            <i class="fa fa-heart-o" aria-hidden="true"></i>
                            <span>
                            Do schowka
                        </span>
                        </a>

                    </div>
                    <div class="col-8 text-right">
                        <div class="badge badge-success">Nowość</div>
                        <div class="badge badge-danger">Promocja</div>
                    </div>
                </div>
            </div>
            <div class="card-body">
                <p class="card-text">Mayne Andrew</p>
                <h5 class="card-title">Naturalista</h5>
                <div class="card-footer">
                    <div class="price">29,99 zł</div>
                    <a href="#" class="position-relative">
                        <img src="./assets/img/trolley.svg" alt="cart icon">
                        <span class="badge badge-custom small">+</span>
                    </a>
                </div>
            </div>
        </div>
        <div class="card">
            <img src="./assets/img/book4.png" class="card-img" alt="...">
            <div class="card-img-overlay">
                <div class="row">
                    <div class="col-4">
                        <a href="#" class="hearth active">
                            <i class="fa fa-heart-o" aria-hidden="true"></i>
                            <span>
                            Do schowka
                        </span>
                        </a>

                    </div>
                    <div class="col-8 text-right">
                        <div class="badge badge-success">Nowość</div>
                        <div class="badge badge-danger">Promocja</div>
                    </div>
                </div>
            </div>
            <div class="card-body">
                <p class="card-text">Mayne Andrew</p>
                <h5 class="card-title">Naturalista</h5>
                <div class="card-footer">
                    <div class="price">29,99 zł</div>
                    <a href="#" class="position-relative">
                        <img src="./assets/img/trolley.svg" alt="cart icon">
                        <span class="badge badge-custom small">+</span>
                    </a>
                </div>
            </div>
        </div>
	</div>
    <div class="arrowSection">
        <div class="row ">
            <div class="offset-lg-4 col-lg-4 slickArrows slickArrows0">
            </div>
            <div class="col-lg-4">
                <a href="#" class="button">Pokaż wszystkie</a>
            </div>
        </div>
    </div>
</div>