<?php
/**
 * Created by PhpStorm.
 * User: dariuszkrol
 * Date: 08/01/2020
 * Time: 16:38
 */?>

<div class="comments">
	<div class="container">
		<h2>Opinie o księgarni XLM</h2>
		<div class="commentsSlider">
			<div class="slide comment">
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </p>
				<div class="author float-left">Mateusz P.</div>
				<div class="rating float-right">
					5.3 / 6.0
					<span class="stars"><span class="dis" style="width:80.00%;"></span></span>
				</div>
			</div>
			<div class="slide comment">
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </p>
				<div class="author float-left">Mateusz P.</div>
				<div class="rating float-right">
					5.3 / 6.0
					<span class="stars"><span class="dis" style="width:80.00%;"></span></span>
				</div>
			</div>
			<div class="slide comment">
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </p>
				<div class="author float-left">Mateusz P.</div>
				<div class="rating float-right">
					5.3 / 6.0
					<span class="stars"><span class="dis" style="width:80.00%;"></span></span>
				</div>
			</div>
			<div class="slide comment">
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </p>
				<div class="author float-left">Mateusz P.</div>
				<div class="rating float-right">
					5.3 / 6.0
					<span class="stars"><span class="dis" style="width:80.00%;"></span></span>
				</div>
			</div>
		</div>
		<div class="arrowSection">
			<div class="row ">
				<div class="offset-lg-4 col-lg-4 slickArrows slickArrowsComments">
				</div>
				<div class="col-lg-3 see-all-button">
					<a href="#" class="button">Pokaż wszystkie</a>
				</div>
			</div>
		</div>
	</div>

</div>
