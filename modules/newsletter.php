<div class="newsletter">
	<div class="container">
		<img class="bell-icon" src="./assets/img/bell.svg" alt="Ikonka dzwonka">
		<h2>Newsletter</h2>
		<p>Zapisz się na newsletter i odbierz DARMOWEGO e-booka.</p>
		<form class="row" action="">
            <div class="col-lg-2 offset-lg-2 ">
                <label class="radio-inline" for="radios-0">
                    <div class="radio-box">
                        <input type="radio" name="radios" id="radios-0" value="1" checked="checked">
                        <span class="checkmark"></span>
                        <span class="radio-box-name">Historia</span>
                    </div>
                    <br>
                    <img src="./assets/img/input1.svg" alt="">
                </label>
            </div>
            <div class="col-lg-2">
			<label class="radio-inline" for="radios-1">
                <div class="radio-box">
				    <input type="radio" name="radios" id="radios-1" value="2">
                    <span class="checkmark"></span>
				    <span class="radio-box-name">Polityka</span>
                </div>
                <br>
                <img src="./assets/img/input2.svg" alt="">
			</label>
            </div>
            <div class="col-lg-2">
			<label class="radio-inline" for="radios-2">
                <div class="radio-box">
				    <input type="radio" name="radios" id="radios-2" value="3">
                    <span class="checkmark"></span>
				    <span class="radio-box-name">Religia</span>
                </div>
                <br>
                <img src="./assets/img/input3.svg" alt="">
			</label>
            </div>
            <div class="col-lg-2">
			<label class="radio-inline display-flex" for="radios-3">
                <div class="radio-box">
				    <input type="radio" name="radios" id="radios-3" value="4">
                    <span class="checkmark"></span>
				    <span class="radio-box-name">Zdrowie</span>
                </div>
                <br>
                <img src="./assets/img/input4.svg" alt="">
			</label>
            </div>
            <div class="col-12 d-flex justify-content-center">
                <div class="newsletter-submit">
                    <label for="email">
                        <input type="email" placeholder="Podaj adres E-mail" required >
                    </label>
                    <button type="submit">Dołącz <i class="fa fa-angle-right"></i></button>
                </div>
            </div>
		</form>
	</div>
</div>