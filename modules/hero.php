<div class="container">
	<div class="row">
		<div class="col-md-9">
			<div class="hero pc-slide ">
				<div class="home-page-slider">

					<div class="slide">
						<img src="./assets/img/hero.png" class="img-slide-mob" />
						<a href=""></a>
						<div class="postInfo d-none d-xl-flex">
							<div class="badge badge-secondary ml-auto">Przedsprzedaż</div>
							<h2>Zarabianie&nbsp; na podróżach</h2>
							<div class="price"><i class="fa fa-angle-left"></i> 79.99 zł</div>
							<p>Karol Wegner
								Wydawnie II 2019 / Ekonomia i biznes</p>
							<a href="#" class="button">Zobacz</a>

						</div>
					</div>
					<div class="slide">
						<img src="./assets/img/hero.png" class="img-slide-mob" />
						<a href=""></a>
					</div>
					<div class="slide">
						<img src="./assets/img/hero.png" class="img-slide-mob" />
						<a href=""></a>
					</div>


				</div>
			</div>
			<div class="mini-menu">
				<div class="slider-nav">

					<div class="slick-element"> Promocje z najwyższej półki. </div>
					<div class="slick-element"> Książki z serii “wszystko o...” </div>
					<div class="slick-element"> Zarabianie na podróżach  </div>
					<div class="slick-element"> Mennica Papieska - sztabki tylko 330 zł  </div>

				</div>
			</div>
		</div>
		<div class="col-md-3 d-md-none d-lg-block">
			<div class="hero pc-slide ">
				<div class="home-page-slider-small">

					<div class="slide small">
						<img src="./assets/img/hero2.png" class="img-slide-mob" />
						<div class="postInfoSmall">
							<h3>Świętuj z nami <br>
								<strong>101 rocznicę</strong> <br>
								niepodległości <br>
							<span>
								101 rabatów!
							</span></h3>
							<a href="#" class="button">Zobacz</a>
						</div>

					</div>
					<div class="slide small">
						<img src="./assets/img/hero2.png" class="img-slide-mob" />
						<div class="postInfoSmall">
							<h3>Świętuj z nami <br>
								<strong>101 rocznicę</strong> <br>
								niepodległości <br>
								<span>
								101 rabatów!
							</span></h3>
							<a href="#" class="button">Zobacz</a>
						</div>

					</div>
					<div class="slide small">
						<img src="./assets/img/hero2.png" class="img-slide-mob" />
						<div class="postInfoSmall">
							<h3>Świętuj z nami <br>
								<strong>101 rocznicę</strong> <br>
								niepodległości <br>
								<span>
								101 rabatów!
							</span></h3>
							<a href="#" class="button">Zobacz</a>
						</div>

					</div>
				</div>
			</div>
		</div>
	</div>
</div>
