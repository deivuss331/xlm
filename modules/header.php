<?php
?>
<!DOCTYPE html>
<html>

<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
	<title>XLM</title>
	<link rel="stylesheet" href="./assets/libs/bootstrap/css/bootstrap.min.css">
    <link href="https://fonts.googleapis.com/css?family=Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i&display=swap&subset=latin-ext" rel="stylesheet">
	<link rel="stylesheet" href="./assets/fonts/font-awesome.min.css">
	<link rel="stylesheet" href="./assets/fonts/ionicons.min.css">
	<link rel="stylesheet" href="./assets/css/styles.css">
	<link rel="stylesheet" href="./assets/libs/slick/slick.css">
	<link rel="stylesheet" href="./assets/libs/slick/slick-theme.css">
</head>
<body class="text-break">
<div class="container-fluid header">
    <nav class="navbar navbar-light navbar-expand-md">
        <div class="container"><a class="navbar-brand" href="#"><img src="./assets/img/logo.svg" alt="cart icon"></a><button data-toggle="collapse" class="navbar-toggler" data-target="#navcol-1"><span class="sr-only">Toggle navigation</span><span class="navbar-toggler-icon"></span></button>
            <form class="form-inline search-form d-none d-xl-flex">
                <input class="form-control search-input" type="search" placeholder="Wpisz nazwe, autora, ISBN, lub wydawnictwo">
                <div class="dropdown">
                    <button class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false" href="#">Wszystkie kategorie&nbsp;</button>
                    <div class="dropdown-menu" role="menu"><a class="dropdown-item" role="presentation" href="#">First Item</a><a class="dropdown-item" role="presentation" href="#">Second Item</a><a class="dropdown-item" role="presentation" href="#">Third Item</a></div>
                </div>
                <button class="btn btn-lg btn-search" type="submit">
                    <svg xmlns="http://www.w3.org/2000/svg" width="16.954" height="17.121" viewBox="0 0 16.954 17.121">
                        <g transform="translate(-1016 34)">
                            <g transform="translate(1016 -34)" fill="none" stroke="#fff" stroke-width="1">
                                <circle cx="7" cy="7" r="7" stroke="none"/>
                                <circle cx="7" cy="7" r="6.5" fill="none"/>
                            </g>
                            <path id="Path_400" data-name="Path 400" d="M1027.5-22.333l5.1,5.1" fill="none" stroke="#fff" stroke-width="1"/>
                        </g>
                    </svg>

                </button>
            </form>
            <div class="collapse navbar-collapse" id="navcol-1">
                <form class="form-inline search-form d-xl-none">
                    <input class="form-control search-input" type="search">
                    <div class="dropdown">
                        <button class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false" href="#" />
                        <div class="dropdown-menu" role="menu"><a class="dropdown-item" role="presentation" href="#">First Item</a><a class="dropdown-item" role="presentation" href="#">Second Item</a><a class="dropdown-item" role="presentation" href="#">Third Item</a></div>
                    </div>
                    <button class="btn btn-lg btn-search" type="submit">
                        <svg xmlns="http://www.w3.org/2000/svg" width="16.954" height="17.121" viewBox="0 0 16.954 17.121">
                            <g transform="translate(-1016 34)">
                                <g transform="translate(1016 -34)" fill="none" stroke="#fff" stroke-width="1">
                                    <circle cx="7" cy="7" r="7" stroke="none"/>
                                    <circle cx="7" cy="7" r="6.5" fill="none"/>
                                </g>
                                <path id="Path_400" data-name="Path 400" d="M1027.5-22.333l5.1,5.1" fill="none" stroke="#fff" stroke-width="1"/>
                            </g>
                        </svg>

                    </button>
                </form>
                <ul class="nav navbar-nav">
                    <li class="nav-item" role="presentation"><a class="nav-link active" href="#"><img src="./assets/img/phone-call.svg"></a></li>
                    <li class="nav-item" id="navbar-item-second" role="presentation"><a class="nav-link active" href="#"><img src="./assets/img/user%20(3).svg"></a></li>
                    <li class="nav-item" id="navbar-item-third" role="presentation">
                        <a class="nav-link active position-relative" href="#">
                            <img src="./assets/img/cart.svg" alt="cart icon">
                            <span class="badge badge-custom">3</span>
                        </a>
                        <div class="dropdown-basket">
                            <div class="dropdown-basket-product">
                                <img src="./assets/img/hero2.png" class="img-slide-mob" />
                                <div class="display-flex dropdown-basket-product-text">
                                    <a href="#">Neuropsychologia medyczna tom 2</a>
                                    <div class="price">29,99 zł</div>
                                </div>
                                <button class="delete-product">
                                    <svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="times" class="svg-inline--fa fa-times fa-w-11" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 352 512"><path fill="#e5e5e5" d="M242.72 256l100.07-100.07c12.28-12.28 12.28-32.19 0-44.48l-22.24-22.24c-12.28-12.28-32.19-12.28-44.48 0L176 189.28 75.93 89.21c-12.28-12.28-32.19-12.28-44.48 0L9.21 111.45c-12.28 12.28-12.28 32.19 0 44.48L109.28 256 9.21 356.07c-12.28 12.28-12.28 32.19 0 44.48l22.24 22.24c12.28 12.28 32.2 12.28 44.48 0L176 322.72l100.07 100.07c12.28 12.28 32.2 12.28 44.48 0l22.24-22.24c12.28-12.28 12.28-32.19 0-44.48L242.72 256z"></path></svg>
                                </button>
                            </div>
                            <div class="dropdown-basket-product">
                                <img src="./assets/img/hero2.png" class="img-slide-mob" />
                                <div class="display-flex dropdown-basket-product-text">
                                    <a href="#">Neuropsychologia medyczna tom 2</a>
                                    <div class="price">29,99 zł</div>
                                </div>
                                <button class="delete-product">
                                    <svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="times" class="svg-inline--fa fa-times fa-w-11" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 352 512"><path fill="#e5e5e5" d="M242.72 256l100.07-100.07c12.28-12.28 12.28-32.19 0-44.48l-22.24-22.24c-12.28-12.28-32.19-12.28-44.48 0L176 189.28 75.93 89.21c-12.28-12.28-32.19-12.28-44.48 0L9.21 111.45c-12.28 12.28-12.28 32.19 0 44.48L109.28 256 9.21 356.07c-12.28 12.28-12.28 32.19 0 44.48l22.24 22.24c12.28 12.28 32.2 12.28 44.48 0L176 322.72l100.07 100.07c12.28 12.28 32.2 12.28 44.48 0l22.24-22.24c12.28-12.28 12.28-32.19 0-44.48L242.72 256z"></path></svg>
                                </button>
                            </div>
                            <div class="dropdown-basket-product">
                                <img src="./assets/img/hero2.png" class="img-slide-mob" />
                                <div class="display-flex dropdown-basket-product-text">
                                    <a href="#">Neuropsychologia medyczna tom 2</a>
                                    <div class="price">29,99 zł</div>
                                </div>
                                <button class="delete-product">
                                    <svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="times" class="svg-inline--fa fa-times fa-w-11" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 352 512"><path fill="#e5e5e5" d="M242.72 256l100.07-100.07c12.28-12.28 12.28-32.19 0-44.48l-22.24-22.24c-12.28-12.28-32.19-12.28-44.48 0L176 189.28 75.93 89.21c-12.28-12.28-32.19-12.28-44.48 0L9.21 111.45c-12.28 12.28-12.28 32.19 0 44.48L109.28 256 9.21 356.07c-12.28 12.28-12.28 32.19 0 44.48l22.24 22.24c12.28 12.28 32.2 12.28 44.48 0L176 322.72l100.07 100.07c12.28 12.28 32.2 12.28 44.48 0l22.24-22.24c12.28-12.28 12.28-32.19 0-44.48L242.72 256z"></path></svg>
                                </button>
                            </div>

                            <div class="dropdown-basket-delivery">
                                <img src="./assets/img/shipping-icon.svg" />
                                <p>Do <span>darmowej dostawy</span>&nbsp; brakuje Tobie jeszcze <span>97.00 zł</span></p>
                            </div>

                            <div class="dropdown-basket-buy">
                                <div class="price">99,99 zł</div>
                                <div class="dropdown-basket-buy-box">
                                    <a href="#">DO KASY</a>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li class="nav-item" role="presentation"><a class="nav-link active" href="#">0,00zł<i class="fa fa-angle-down"></i></a></li>
                </ul>
            </div>
        </div>
    </nav>
</div>
<div class="container">
    <ul class="top-categories nav nav-tabs d-flex flex-row flex-grow-1 flex-shrink-1 flex-fill justify-content-start main-nav">
        <li class="nav-item"><a class="nav-link" href="#">KSIĄŻKI</a></li>
        <li class="nav-item"><a class="nav-link" href="#">AUDIOBOOKI</a></li>
        <li class="nav-item"><a class="nav-link" href="#">E-BOOKI</a></li>
        <li class="nav-item"><a class="nav-link" href="#">BESTSELLERY</a></li>
        <li class="nav-item"><a class="nav-link" href="#">NOWOŚCI</a></li>
        <li class="nav-item"><a class="nav-link" href="#">PROMOCJE</a></li>
    </ul>
    <ul class="nav nav-tabs d-flex justify-content-center align-items-lg-center sub-menu">
        <li class="nav-item"><a class="nav-link" href="#">Ekonomia / biznes</a></li>
        <li class="nav-item"><a class="nav-link" href="#">Filozofia / etyka</a></li>
        <li class="nav-item"><a class="nav-link" href="#">&nbsp;Historia / arecheologia</a></li>
        <li class="nav-item"><a class="nav-link" href="#">&nbsp;Nauki ścisłe</a></li>
        <li class="nav-item"><a class="nav-link" href="#">&nbsp;Medycyna</a></li>
        <li class="nav-item"><a class="nav-link" href="#">&nbsp;Polityka / politologia</a></li>
        <li class="nav-item"><a class="nav-link" href="#">&nbsp;Popularnonaukowe</a></li>
        <li class="nav-item"><a class="nav-link" href="#">&nbsp;Religijne</a></li>
    </ul>
</div>