<?php
include 'modules/header.php';?>
<div class="login-page">
<?
include 'modules/breadcrumbs.php';
?>
<div class="container">
	<hr>
	<h1>Logowanie do sklepu</h1>
</div>
<div class="content">
	<div class="container">
		<div class="row ">
			<div class="col-md-4">
				<div class="content-section">
					<h3>Logowanie</h3>
					<form action="">
						<div class="form-group">
							<input name="" class="form-control" placeholder="Email" type="email">
						</div> <!-- form-group// -->
						<div class="form-group">
							<input class="form-control" placeholder="Hasło" type="password">
						</div> <!-- form-group// -->
						<a class="" href="#">Przypomnij hasło</a>
						<div class="form-group">
							<button type="submit" class="button "> Zaloguj do sklepu  </button>
						</div> <!-- form-group// -->
					</form>
				</div>
			</div>
			<div class="col-md-8">
				<div class="content-section">
					<h3>Zakładanie konta w sklepie</h3>
					<form action="">
						<div class="row register-form">
							<div class="col-12 d-flex">
								<div class="form-check d-flex align-items-center">
									<input class="form-check-input" type="checkbox" value="" id="defaultCheck3r">
									<label class="check-box form-check-label" for="defaultCheck3r">
									</label>
									<div class="label">Klient indywidualny</div>
								</div>
								<div class="form-check d-flex align-items-center ml-3">
									<input class="form-check-input" type="checkbox" value="" id="defaultCheck3ra">
									<label class="check-box form-check-label" for="defaultCheck3ra">
									</label>
									<div class="label">Klient biznesowy</div>
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<input type="text" class="form-control" placeholder="Imię" value="" />
								</div>
								<div class="form-group">
									<input type="text" class="form-control" placeholder="Nazwisko" value="" />
								</div>
								<div class="form-group">
									<input type="tel" class="form-control" placeholder="+48" value="" />
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<input type="email" class="form-control" placeholder="Email" value="" />
								</div>
								<div class="form-group">
									<input type="password" class="form-control" placeholder="Hasło" value="" />
								</div>
								<div class="form-group">
									<input type="password" class="form-control" placeholder="Powtórz hasło" value="" />
								</div>

							</div>
							<div class="col-12">
								<div class="form-check">
									<input class="form-check-input" type="checkbox" value="" id="defaultCheck3">
									<label class="check-box form-check-label" for="defaultCheck3">
									</label>
									<div class="label">Wyrażam zgodę na przetwarzanie przez firmę Fronda PL Sp. z o. o. z siedzibą w Warszawie (02-220), ul. Łopuszańska 32 (zwaną dalej "XLM") moich danych osobowych w celu realizacji zamówień składanych przez Sklep Internetowy XLM.pl. Zostałem poinformowany, iż administratorem moich danych osobowych jest XLM oraz o przysługującym mi prawie dostępu do moich danych osobowych, prawie ich poprawiania lub usunięcia, a także o celu zbierania danych osobowych przez XLM oraz dobrowolności ich podania. Oświadczam, że jestem osobą pełnoletnią.</div>
								</div>
								<div class="form-check">
									<input class="form-check-input" type="checkbox" value="" id="defaultCheck">
									<label class="check-box form-check-label" for="defaultCheck">
									</label>
									<div class="label">chcę otrzymywać newsletter
									</div>
								</div>
								<div class="form-group">
									<button type="submit" class="button whiteButton  w-100 d-block"> Załóż konto  </button>
								</div> <!-- form-group// -->
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
<?php
include 'modules/footer.php';?>